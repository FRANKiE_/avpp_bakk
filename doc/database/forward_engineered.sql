SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `AngioVisPatientPersistency` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `AngioVisPatientPersistency` ;

-- -----------------------------------------------------
-- Table `AngioVisPatientPersistency`.`FileType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AngioVisPatientPersistency`.`FileType` ;

CREATE  TABLE IF NOT EXISTS `AngioVisPatientPersistency`.`FileType` (
  `idFileType` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`idFileType`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AngioVisPatientPersistency`.`Patient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AngioVisPatientPersistency`.`Patient` ;

CREATE  TABLE IF NOT EXISTS `AngioVisPatientPersistency`.`Patient` (
  `idPatient` INT NOT NULL AUTO_INCREMENT ,
  `pID` VARCHAR(255) NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`idPatient`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AngioVisPatientPersistency`.`Study`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AngioVisPatientPersistency`.`Study` ;

CREATE  TABLE IF NOT EXISTS `AngioVisPatientPersistency`.`Study` (
  `idStudy` INT NOT NULL AUTO_INCREMENT ,
  `uid` VARCHAR(255) NOT NULL ,
  `description` VARCHAR(255) NOT NULL ,
  `date` DATE NOT NULL ,
  `time` TIME NOT NULL ,
  `Patient_idPatient` INT NOT NULL ,
  PRIMARY KEY (`idStudy`) ,
  INDEX `fk_Study_Patient1_idx` (`Patient_idPatient` ASC) ,
  CONSTRAINT `fk_Study_Patient1`
    FOREIGN KEY (`Patient_idPatient` )
    REFERENCES `AngioVisPatientPersistency`.`Patient` (`idPatient` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AngioVisPatientPersistency`.`Series`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AngioVisPatientPersistency`.`Series` ;

CREATE  TABLE IF NOT EXISTS `AngioVisPatientPersistency`.`Series` (
  `idSeries` INT NOT NULL AUTO_INCREMENT ,
  `uid` VARCHAR(255) NOT NULL ,
  `description` VARCHAR(255) NOT NULL ,
  `date` DATE NOT NULL COMMENT 'evaluate usage of TZ on date and time' ,
  `time` TIME NOT NULL ,
  `FileType_idFileType` INT NOT NULL ,
  `Study_idStudy` INT NOT NULL ,
  PRIMARY KEY (`idSeries`) ,
  INDEX `fk_Series_FileType1_idx` (`FileType_idFileType` ASC) ,
  INDEX `fk_Series_Study1_idx` (`Study_idStudy` ASC) ,
  CONSTRAINT `fk_Series_FileType1`
    FOREIGN KEY (`FileType_idFileType` )
    REFERENCES `AngioVisPatientPersistency`.`FileType` (`idFileType` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Series_Study1`
    FOREIGN KEY (`Study_idStudy` )
    REFERENCES `AngioVisPatientPersistency`.`Study` (`idStudy` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `AngioVisPatientPersistency`.`File`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `AngioVisPatientPersistency`.`File` ;

CREATE  TABLE IF NOT EXISTS `AngioVisPatientPersistency`.`File` (
  `idFile` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `size` BIGINT NOT NULL ,
  `FileType_idFileType` INT NOT NULL ,
  `Series_idSeries` INT NOT NULL ,
  INDEX `fk_File_FileType_idx` (`FileType_idFileType` ASC) ,
  PRIMARY KEY (`idFile`) ,
  INDEX `fk_File_Series1_idx` (`Series_idSeries` ASC) ,
  CONSTRAINT `fk_File_FileType`
    FOREIGN KEY (`FileType_idFileType` )
    REFERENCES `AngioVisPatientPersistency`.`FileType` (`idFileType` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_File_Series1`
    FOREIGN KEY (`Series_idSeries` )
    REFERENCES `AngioVisPatientPersistency`.`Series` (`idSeries` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `AngioVisPatientPersistency` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

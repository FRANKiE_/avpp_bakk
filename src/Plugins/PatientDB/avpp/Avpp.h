#pragma once

#include <qvariant.h>

#define CONFIG_PATH "./config.json"

#include <vector>
#include <map>

#include "util/qslog/QsLog.h"

namespace avpp
{
    enum ENTITY_TYPE
    {
        PATIENT = 0,
        STUDY,
        SERIES,
        FILE,
        FILETYPE
    };

    enum DAO_TYPE
    {
        SQL = 0
    };

    namespace ent
    {
        enum EntityStatus
        {
            STUB = 0,
            LOADED,
            DIRTY
        };

        enum EntityLoadingMode
        {
            LAZY = 0,
            EAGER
        };

        class Patient;
        class Study;
        class Series;
        class File;
        class FileType;
    }

    typedef std::map<int, ent::Patient*> PatientMap;
    typedef std::map<int, ent::Study*> StudyMap;
    typedef std::map<int, ent::Series*> SeriesMap;
    typedef std::map<int, ent::File*> FileMap;
    typedef std::map<int, ent::FileType*> FileTypeMap;

    typedef std::vector<ent::Patient*> PatientList;
    typedef std::vector<ent::Study*> StudyList;
    typedef std::vector<ent::Series*> SeriesList;
    typedef std::vector<ent::File*> FileList;
    typedef std::vector<ent::FileType*> FileTypeList;

    typedef std::map<int, StudyList> PatientStudyMap;
    typedef std::map<int, SeriesList> StudySeriesMap;
    typedef std::map<int, FileList> SeriesFileMap;
}

#include "Logging.h"

#include "util/Settings.h"
#include "util/qslog/QsLogDest.h"

#include <QDir>

using namespace avpp;
using namespace avpp::util;

Logging::Logging() {}

void Logging::setup()
{
	// read logfile path from configfile with a fallback path (just in case)
	Settings settings(CONFIG_PATH);
	bool settingsOk = settings.load();

	QString logFilePath = QDir::currentPath() + "default.log";

	if (settingsOk)
	{
		logFilePath = settings.getResult().toMap()["Logging"].toMap()["logFilePath"].toString();
	}

	// secure logging directory exists
	QDir dir;
	dir.mkpath(logFilePath.section("/", 0, -2));

	// setup logging
	QsLogging::Logger &logger = QsLogging::Logger::instance();
	logger.setLoggingLevel(QsLogging::DebugLevel);

	/*
	 * start rotation after 512MB
	 * keep 50 logfiles
	 */
	QsLogging::DestinationPtr fileDestination(QsLogging::DestinationFactory::MakeFileDestination(logFilePath, true, 536870912, 50));
	QsLogging::DestinationPtr debugDestination(QsLogging::DestinationFactory::MakeDebugOutputDestination());

	logger.addDestination(fileDestination);
	logger.addDestination(debugDestination);

	QLOG_INFO() << "Logging::setup - initialized logging framework successfully";
}

#include "Manager.h"
#include "PsqlConnection.h"
#include "Logging.h"

#include "dao/PatientDao.h"
#include "dao/StudyDao.h"
#include "dao/SeriesDao.h"
#include "dao/FileDao.h"
#include "dao/FileTypeDao.h"

#include "dao/PatientDaoSql.h"
#include "dao/StudyDaoSql.h"
#include "dao/SeriesDaoSql.h"
#include "dao/FileDaoSql.h"
#include "dao/FileTypeDaoSql.h"

#include "ent/File.h"
#include "ent/FileType.h"
#include "ent/Patient.h"
#include "ent/Series.h"
#include "ent/Study.h"

#include <algorithm>
#include <functional>

using namespace avpp;

Manager* Manager::instance = nullptr;

Manager::Manager() :
    patientMap(),
    studyMap(),
    seriesMap(),
    fileMap(),
    fileTypeMap(),
    patientStudyMatches(),
    studySeriesMatches(),
    seriesFileMatches(),
    connection(nullptr),
    daoType(),
    patientDao(nullptr),
    studyDao(nullptr),
    seriesDao(nullptr),
    fileDao(nullptr),
    fileTypeDao(nullptr)
{
    init(SQL);
}

Manager::Manager(DAO_TYPE daoType) :
    patientMap(),
    studyMap(),
    seriesMap(),
    fileMap(),
    fileTypeMap(),
    patientStudyMatches(),
    studySeriesMatches(),
    seriesFileMatches(),
    connection(nullptr),
    daoType(),
    patientDao(nullptr),
    studyDao(nullptr),
    seriesDao(nullptr),
    fileDao(nullptr),
    fileTypeDao(nullptr)
{
    init(daoType);
}

Manager::~Manager()
{
    if (this->connection->isOpen())
    {
        this->connection->close();
    }

    delete this->connection;

    /*
     * iterate over local cache to delete all entities
     */
    for (PatientMap::const_iterator it = this->patientMap.begin(); it != this->patientMap.end(); ++it)
    {
        delete it->second;
    }

    for (StudyMap::const_iterator it = this->studyMap.begin(); it != this->studyMap.end(); ++it)
    {
        delete it->second;
    }

    for (SeriesMap::const_iterator it = this->seriesMap.begin(); it != this->seriesMap.end(); ++it)
    {
        delete it->second;
    }

    for (FileMap::const_iterator it = this->fileMap.begin(); it != this->fileMap.end(); ++it)
    {
        delete it->second;
    }
}

Manager* Manager::getInstance()
{
    if (Manager::instance == nullptr)
    {
        Manager::instance = new Manager();
    }

    return Manager::instance;
}

void Manager::cleanInstance()
{
    if (Manager::instance != nullptr)
    {
        QLOG_INFO() << "Manager::cleanInstance - shutdown sequence initialized, cleaning up manager...";

        Manager::instance->connection->close();

        delete Manager::instance;
        Manager::instance = nullptr;
    }
}

void Manager::init(DAO_TYPE daoType)
{
    this->daoType = daoType;

    Logging logging;
    logging.setup();

    switch (daoType)
    {
    default:
    case SQL:
        this->connection = new PsqlConnection();
        break;
    }

    this->connection->open();

    this->fileTypeList = this->getSpecificFileTypeDao()->getAllFileTypes();
    for (FileTypeList::iterator fti = this->fileTypeList.begin(); fti != this->fileTypeList.end(); ++fti)
    {
        ent::FileType* newFileType = *fti;
        this->fileTypeMap[newFileType->getId()] = newFileType;
    }
}

bool Manager::isReady()
{
    return this->connection->isOpen();
}

void Manager::assignNewSpecificPatientDao()
{
    switch (this->daoType)
    {
    case SQL:
    default:
        this->patientDao = new dao::PatientDaoSql(this);
        break;
    }
}

dao::PatientDao* Manager::getSpecificPatientDao()
{
    if (this->patientDao == nullptr)
    {
        this->assignNewSpecificPatientDao();
    }
    else if (this->daoType != this->patientDao->getDaoType())
    {
        delete this->patientDao;
        this->assignNewSpecificPatientDao();
    }

    return this->patientDao;
}

void Manager::assignNewSpecificStudyDao()
{
    switch (this->daoType)
    {
    case SQL:
    default:
        this->studyDao = new dao::StudyDaoSql(this);
        break;
    }
}

dao::StudyDao* Manager::getSpecificStudyDao()
{
    if (this->studyDao == nullptr)
    {
        this->assignNewSpecificStudyDao();
    }
    else if (this->daoType != this->studyDao->getDaoType())
    {
        delete this->studyDao;
        this->assignNewSpecificStudyDao();
    }

    return this->studyDao;
}

void Manager::assignNewSpecificSeriesDao()
{
    switch (this->daoType)
    {
    default:
    case SQL:
        this->seriesDao = new dao::SeriesDaoSql(this);
        break;
    }
}

dao::SeriesDao* Manager::getSpecificSeriesDao()
{
    if (this->seriesDao == nullptr)
    {
        this->assignNewSpecificSeriesDao();
    }
    else if (this->daoType != this->seriesDao->getDaoType())
    {
        delete this->seriesDao;
        this->assignNewSpecificSeriesDao();
    }

    return this->seriesDao;
}

void Manager::assignNewSpecificFileDao()
{
    switch (this->daoType)
    {
    default:
    case SQL:
        this->fileDao = new dao::FileDaoSql(this);
        break;
    }
}

dao::FileDao* Manager::getSpecificFileDao()
{
    if (this->fileDao == nullptr)
    {
        this->assignNewSpecificFileDao();
    }
    else if (this->daoType != this->fileDao->getDaoType())
    {
        delete this->fileDao;
        this->assignNewSpecificFileDao();
    }

    return this->fileDao;
}

void Manager::assignNewSpecificFileTypeDao()
{
    switch (this->daoType)
    {
    default:
    case SQL:
        this->fileTypeDao = new dao::FileTypeDaoSql(this);
        break;
    }
}

dao::FileTypeDao* Manager::getSpecificFileTypeDao()
{
    if (this->fileTypeDao == nullptr)
    {
        this->assignNewSpecificFileTypeDao();
    }
    else if (this->daoType != this->fileTypeDao->getDaoType())
    {
        delete this->fileTypeDao;
        this->assignNewSpecificFileTypeDao();
    }

    return this->fileTypeDao;
}

ent::Patient* Manager::loadPatientStub(ent::Study* study)
{
    if (study == nullptr)
    {
        QLOG_WARN() << "Manager::loadPatientStub - passed study was nullptr";
        return nullptr;
    }

    ent::Patient* patient = this->getSpecificPatientDao()->getPatientStubByStudy(study);

    if (patient == nullptr)
    {
        QLOG_WARN() << "Manager::loadPatientStub - read patient stub was nullptr";
        return nullptr;
    }

    // check if matching from patient to study is already done
    PatientStudyMap::iterator psmi = this->patientStudyMatches.find(patient->getId());

    // no match from patient to study found
    if (psmi == this->patientStudyMatches.end())
    {
        // insert new list of studies per patient
        StudyList studyList;
        studyList.push_back(study);
        this->patientStudyMatches[patient->getId()] = studyList;
    }
    else
    {
        // append study to already known matches
        StudyList studyList = this->patientStudyMatches[patient->getId()];
        studyList.push_back(study);
    }

    // is the patient already cached?
    PatientMap::iterator pmi = this->patientMap.find(patient->getId());

    // no patient cached yet
    if (pmi == this->patientMap.end())
    {
        // write patient stub to cache
        this->patientMap[patient->getId()] = patient;
        study->setPatient(patient);
        return patient;
    }
    else
    {
        // assign already cached patient to study
        ent::Patient* cachedPatient = this->patientMap[patient->getId()];
        study->setPatient(cachedPatient);
        delete patient;
        return cachedPatient;
    }
}

void Manager::loadStudyStubs(ent::Patient* patient)
{
    if (patient == nullptr)
    {
        QLOG_WARN() << "Manager::loadStudyStubs - passed patient was nullptr";
        return;
    }

    StudyList stubs = this->getSpecificStudyDao()->getAllStudyStubsByPatient(patient);

    if (patient == nullptr)
    {
        QLOG_WARN() << "Manager::loadStudyStubs - read study stub was nullptr";
        return;
    }

    // already loaded stubs?
    PatientStudyMap::iterator psmi = this->patientStudyMatches.find(patient->getId());
    bool initiallySetMatches = psmi == this->patientStudyMatches.end();
    if (initiallySetMatches)
    {
        // write the loaded stubs directly
        this->patientStudyMatches[patient->getId()] = stubs;
    }

    StudyList actualStudyMatches = this->patientStudyMatches[patient->getId()];

    // add stubs if no entries for this stub are found
    for (StudyList::const_iterator ci = stubs.begin(); ci != stubs.end(); ci++)
    {
        ent::Study* stub = *ci;

        // update actual study cache (StudyMap)
        StudyMap::const_iterator mi = this->studyMap.find(stub->getId());
        if (mi == this->studyMap.end())
        {
            this->studyMap[stub->getId()] = stub;
        }

        if (!initiallySetMatches)
        {
            // update matches from patient to study (PatientStudyMap)
            StudyList::const_iterator li = std::find_if(actualStudyMatches.begin(),
                                                        actualStudyMatches.end(),
                                                        std::bind1st(std::mem_fun(&ent::Entity::comparePredicate), stub)
                                                        );

            if (li == actualStudyMatches.end())
            {
                actualStudyMatches.push_back(stub);
            }
        }
    }
}

ent::Study* Manager::loadStudyStub(ent::Series* series)
{
    if (series == nullptr)
    {
        QLOG_WARN() << "Manager::loadStudyStub - passed series was nullptr";
        return nullptr;
    }

    ent::Study* study = this->getSpecificStudyDao()->getStudyStubBySeries(series);

    if (study == nullptr)
    {
        QLOG_WARN() << "Manager::loadStudyStub - read study stub was nullptr";
        return nullptr;
    }

    // check if matching from study to series is already done
    StudySeriesMap::iterator ssmi = this->studySeriesMatches.find(study->getId());

    // no match from study to series found
    if (ssmi == this->studySeriesMatches.end())
    {
        // insert new list of series' per study
        SeriesList seriesList;
        seriesList.push_back(series);
        this->studySeriesMatches[study->getId()] = seriesList;
    }
    else
    {
        // append series to already known matches
        SeriesList seriesList = this->studySeriesMatches[study->getId()];
        seriesList.push_back(series);
    }

    // is the study already cached?
    StudyMap::iterator smi = this->studyMap.find(study->getId());

    // no study cached yet
    if (smi == this->studyMap.end())
    {
        // write study stub to cache
        this->studyMap[study->getId()] = study;
        series->setStudy(study);
        return study;
    }
    else
    {
        // assign already cached study to series
        ent::Study* cachedStudy = this->studyMap[study->getId()];
        series->setStudy(cachedStudy);
        delete study;
        return cachedStudy;
    }
}

void Manager::loadSeriesStubs(ent::Study* study)
{
    if (study == nullptr)
    {
        QLOG_WARN() << "Manager::loadSeriesStubs - passed study was nullptr";
        return;
    }

    SeriesList stubs = this->getSpecificSeriesDao()->getAllSeriesStubsByStudy(study);

    // already loaded stubs?
    StudySeriesMap::iterator ssmi = this->studySeriesMatches.find(study->getId());
    bool initiallySetMatches = ssmi == this->studySeriesMatches.end();
    if (initiallySetMatches)
    {
        // write the loaded stubs directly
        this->studySeriesMatches[study->getId()] = stubs;
    }

    SeriesList actualSeriesMatches = this->studySeriesMatches[study->getId()];

    // add stubs if no entries for this stub are found
    for (SeriesList::const_iterator ci = stubs.begin(); ci != stubs.end(); ci++)
    {
        ent::Series* stub = *ci;

        // update actual series cache (SeriesMap)
        SeriesMap::const_iterator mi = this->seriesMap.find(stub->getId());
        if (mi == this->seriesMap.end())
        {
            this->seriesMap[stub->getId()] = stub;
        }

        if (!initiallySetMatches)
        {
            // update matches from study to series (StudySeriesMap)
            SeriesList::iterator li = std::find_if(actualSeriesMatches.begin(),
                                                   actualSeriesMatches.end(),
                                                   std::bind1st(std::mem_fun(&ent::Entity::comparePredicate), stub)
                                                   );
            if (li == actualSeriesMatches.end())
            {
                actualSeriesMatches.push_back(stub);
            }
        }
    }
}

ent::Series* Manager::loadSeriesStub(ent::File* file)
{
    if (file == nullptr)
    {
        QLOG_WARN() << "Manager::loadSeriesStub - passed file was nullptr";
        return nullptr;
    }

    ent::Series* series = this->getSpecificSeriesDao()->getSeriesStubByFile(file);

    if (series == nullptr)
    {
        QLOG_WARN() << "Manager::loadSeriesStub - read series stub was nullptr";
        return nullptr;
    }

    if (series == nullptr)
    {
        return series;
    }

    // check if matching from series to file is already done
    SeriesFileMap::iterator sfmi = this->seriesFileMatches.find(series->getId());

    // no match from series to file found
    if (sfmi == this->seriesFileMatches.end())
    {
        // insert new list of file per series
        FileList fileList;
        fileList.push_back(file);
        this->seriesFileMatches[series->getId()] = fileList;
    }
    else
    {
        // append file to already known matches
        FileList fileList = this->seriesFileMatches[series->getId()];
        fileList.push_back(file);
    }

    // is the series already cached?
    SeriesMap::iterator smi = this->seriesMap.find(series->getId());

    // no series cached yet
    if (smi == this->seriesMap.end())
    {
        // write series stub to cache
        this->seriesMap[series->getId()] = series;
        file->setSeries(series);
        return series;
    }
    else
    {
        // assign already cached series to file
        ent::Series* cachedSeries = this->seriesMap[series->getId()];
        file->setSeries(cachedSeries);
        delete series;
        return cachedSeries;
    }
}

void Manager::loadFileStubs(ent::Series* series)
{
    if (series == nullptr)
    {
        QLOG_WARN() << "Manager::loadSeriesStub - passed series was nullptr";
        return;
    }

    FileList stubs = this->getSpecificFileDao()->getAllFileStubsBySeries(series);

    // already loaded stubs?
    SeriesFileMap::iterator sfmi = this->seriesFileMatches.find(series->getId());
    bool initiallySetMatches = sfmi == this->seriesFileMatches.end();
    if (initiallySetMatches)
    {
        // write the loaded stubs directly
        this->seriesFileMatches[series->getId()] = stubs;
    }

    FileList actualFileMatches = this->seriesFileMatches[series->getId()];

    // add stubs if no entries for this stub are found
    for (FileList::const_iterator ci = stubs.begin(); ci != stubs.end(); ci++)
    {
        ent::File* stub = *ci;

        // update actual file cache (FileMap)
        FileMap::const_iterator mi = this->fileMap.find(stub->getId());
        if (mi == this->fileMap.end())
        {
            this->fileMap[stub->getId()] = stub;
        }

        if (!initiallySetMatches)
        {
            // update matches from series to file (SeriesFileMap)
            FileList::iterator li = std::find_if(actualFileMatches.begin(),
                                                 actualFileMatches.end(),
                                                 std::bind1st(std::mem_fun(&ent::Entity::comparePredicate), stub)
                                                 );

            if (li == actualFileMatches.end())
            {
                actualFileMatches.push_back(stub);
            }
        }
    }
}

PatientList Manager::getAllPatients()
{
    PatientList temporaryPatientList = this->getSpecificPatientDao()->getAllPatients();

    /*
     * iterate over all patients to update local cache and so on
     */
    for (avpp::PatientList::iterator ci = temporaryPatientList.begin(); ci != temporaryPatientList.end(); ci++)
    {
        ent::Patient* newPatient = *ci;

        /*
         * does local patient cache alread contain patient?
         */
        avpp::PatientMap::const_iterator mi = this->patientMap.find(newPatient->getId());
        if (mi == this->patientMap.end())
        {
            /*
             * patient currently not cached, add + load dependent stubs
             */
            QLOG_INFO() << "Manager::getAllPatients - patient (#" << newPatient->getId() << ") not yet cached, loading into cache";
            this->patientMap[newPatient->getId()] = newPatient;
            QLOG_INFO() << "Manager::getAllPatients - loading stubs for patient (#" << newPatient->getId() << ")...";
            this->loadStudyStubs(newPatient);
        }
        else
        {
            /*
             * patient already in cache, retrieve current version
             */
            ent::Patient* currentlyLoadedPatient = this->patientMap[newPatient->getId()];

            if (currentlyLoadedPatient->getStatus() == ent::STUB)
            {
                /*
                 * currently cached patient was only a stub, update (not break pointer
                 * integrity here) with loaded patient and load dependent stubs
                 */
                QLOG_INFO() << "Manager::getAllPatients - cached patient (#" << currentlyLoadedPatient->getId() << ") was only a stub, updating...";
                ent::Patient* stubbedPatient = this->patientMap[currentlyLoadedPatient->getId()];
                stubbedPatient->update(newPatient);
                QLOG_INFO() << "Manager::getAllPatients - loading study stubs for patient...";
                this->loadStudyStubs(stubbedPatient);
            }
            else
            {
                QLOG_INFO() << "Manager::getAllPatients - already cached patient (#" << newPatient->getId() << ") loaded";
                /*
                 * check if current patient is dirty or loaded
                 */
                if (currentlyLoadedPatient->getStatus() == ent::DIRTY)
                {
                    /*
                     * discard loaded version to prevent loss of dirty data
                     * assumption, stubs were already loaded
                     */
                    QLOG_WARN() << "Manager::getAllPatients - discareded patient (#" << newPatient->getId() << ") local version is dirty";
                    *ci = currentlyLoadedPatient;
                }
                else
                {
                    /*
                     * update currently loaded patient (don't break pointer integrity here)
                     * with updated values check if stubs were already loaded and load them
                     * if not
                     */
                    ent::Patient* loadedPatient = this->patientMap[currentlyLoadedPatient->getId()];
                    loadedPatient->update(newPatient);

                    PatientStudyMap::const_iterator studyMatches = this->patientStudyMatches.find(loadedPatient->getId());
                    if (studyMatches == this->patientStudyMatches.end())
                    {
                        this->loadStudyStubs(loadedPatient);
                    }
                    else
                    {
                        QLOG_INFO() << "Manager::getAllPatients - study stubs for patient (#" << loadedPatient->getId() << ") already loaded";
                    }
                }
            }
        }
    }

    return temporaryPatientList;
}

ent::Patient* Manager::getPatientById(int id, ent::EntityLoadingMode mode)
{
    avpp::PatientMap::iterator mi = this->patientMap.find(id);

    // not in cache
    if (mi == this->patientMap.end())
    {
        // Load Patient into cache
        QLOG_INFO() << "Manager::getPatientById - patient (#" << id << ") not yet cached, loading...";
        ent::Patient* patient = this->getSpecificPatientDao()->getPatientById(id);

        if (patient != nullptr)
        {
            patient->setLoaded();
            this->patientMap[patient->getId()] = patient;
            QLOG_INFO() << "Manager::getPatientById - cached new patient, loading stubs...";
            this->loadStudyStubs(patient);
            QLOG_INFO() << "Manager::getPatientById - finished loading patient and stubs";
        }
        else
        {
            QLOG_WARN() << "Manager::getPatientById - failed to load new patient";
        }

        return patient;
    }
    else
    {
        // Patient already in cache
        ent::Patient* patient = this->patientMap[id];
        if (patient->getStatus() == ent::STUB)
        {
            QLOG_INFO() << "Manager::getPatientById - Patient (#" << id << ") is a stub, loading conetnts...";
            ent::Patient* loadedPatient = this->getSpecificPatientDao()->getPatientById(id);

            if (loadedPatient != nullptr)
            {
                patient->update(loadedPatient);
                patient->setLoaded();
                QLOG_INFO() << "Manager::getPatientById - updated stubbed patient, loading stubs...";
                this->loadStudyStubs(patient);
                QLOG_INFO() << "Manager::getPatientById - finished loading patient and stubs";
            }
            else
            {
                QLOG_WARN() << "Manager::getPatientById - failed to update stubbed patient";
            }

            return patient;
        }
        else
        {
            if (patient->getStatus() == ent::DIRTY)
            {
                this->loadStudyStubs(patient);
                QLOG_INFO() << "Manager::getPatientById - Patient (#" << patient->getId() << ") is dirty, loadStudyStubs";
                return patient;
            }
            else // patient is ent::LOADED
            {
                if (mode == ent::EAGER)
                {
                    QLOG_INFO() << "Manager::getPatientById - updating patient (#" << id << ") ...";
                    ent::Patient* loadedPatient = this->getSpecificPatientDao()->getPatientById(id);

                    if (loadedPatient != nullptr)
                    {
                        patient->update(loadedPatient);
                        QLOG_INFO() << "Manager::getPatientById - updated cached patient, loading stubs...";
                        this->loadStudyStubs(patient);
                        QLOG_INFO() << "Manager::getPatientById - finished loading patient and stubs";
                    }
                    else
                    {
                        QLOG_WARN() << "Manager::getPatientById - failed to update cached patient";
                    }

                    return patient;
                }
                else // mode == ent::LAZY
                {
                    return patient;
                }
            }
        }

    }
}

ent::Patient* Manager::getPatientByPid(QString pID)
{
	return this->getSpecificPatientDao()->getPatientByPid(pID);
}

ent::Patient* Manager::getPatientByStudy(ent::Study* study, ent::EntityLoadingMode mode)
{
    if (study == nullptr)
    {
        QLOG_WARN() << "Manager::getPatientByStudy - passed study was nullptr";
        return nullptr;
    }

    // study already knows it's patient?
    if (study->getPatient() != nullptr)
    {
        return this->getPatientById(study->getPatient()->getId());
    }
    else
    {
        ent::Patient* patient = this->loadPatientStub(study);

        if (mode == ent::EAGER && patient != nullptr)
        {
            patient->update(this->getPatientById(patient->getId()));
        }

        return patient;
    }
}

bool Manager::insertPatient(ent::Patient* patient)
{
    if (patient == nullptr)
    {
        QLOG_WARN() << "Manager::insertPatient - passed patient was nullptr";
        return false;
    }

    // patient already cached?
    avpp::PatientMap::iterator mi = this->patientMap.find(patient->getId());
    if (mi == this->patientMap.end())
    {
        /*
         * Load Patient into cache and insert into database
         */
        QLOG_INFO() << "Manager::insertPatient - patient (#" << patient->getId() << ") not cached, inserting...";
        if (this->getSpecificPatientDao()->insertPatient(patient))
        {
            patient->setLoaded();
            this->patientMap[patient->getId()] = patient;
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        /*
         * wenn er in der map ist und ich bekomm den paramtere als loaded hinein,
         * dann überreiche ich ihn ans update weiter
         */
        if (patient->getStatus() == ent::LOADED)
        {
            // forward to update function
            return this->updatePatient(patient);
        }
        // 1_WARNING revisit if dirty needs to be handled
    }

    return false;
}

bool Manager::updatePatient(ent::Patient* patient)
{
    if (patient == nullptr)
    {
        QLOG_WARN() << "Manager::updatePatient - passed patient was nullptr";
        return false;
    }

    // patient already in cache and not dirty
    PatientMap::iterator pmi = this->patientMap.find(patient->getId());
    if (pmi != this->patientMap.end())
    {
        ent::Patient* cachedPatient = this->patientMap[patient->getId()];
        // if patient is not dirty
        if (cachedPatient->getStatus() != ent::DIRTY &&
                this->getSpecificPatientDao()->updatePatient(patient))
        {
            cachedPatient->setLoaded();
            return true;
        }
    }

    return false;
}

bool Manager::deletePatient(ent::Patient* patient)
{
    if (patient == nullptr)
    {
        QLOG_WARN() << "Manager::deletePatient - passed patient was nullptr";
        return false;
    }

    // patient has matches to study?
    StudyList sl = this->patientStudyMatches[patient->getId()];
    if (sl.size() == 0)
    {
        this->patientStudyMatches.erase(patient->getId());

        // acutally delete the patient
        if (this->getSpecificPatientDao()->deletePatient(patient))
        {
            this->patientMap.erase(patient->getId());
            return true;
        }
    }

    return false;
}

StudyList Manager::getAllStudiesByPatient(ent::Patient* patient)
{
    if (patient == nullptr)
    {
        QLOG_WARN() << "Manager::getAllStudiesByPatient - passed patient was nullptr";
        return StudyList();
    }

    StudyList loadedStudies = this->getSpecificStudyDao()->getAllStudiesByPatient(patient);

    // studies already cached in matches
    StudyList currentStudies = this->patientStudyMatches[patient->getId()];
    if (currentStudies.size() == 0)
    {
        // not cached yet just write loadedStudies list directly into matches and preload series stubs
        this->patientStudyMatches.erase(patient->getId());
        currentStudies = loadedStudies;
        this->patientStudyMatches[patient->getId()] = currentStudies;

        for (StudyList::const_iterator sli = currentStudies.begin(); sli != currentStudies.end(); sli++)
        {
            this->loadSeriesStubs(*sli);
        }
    }
    else
    {
        for (StudyList::const_iterator sli = loadedStudies.begin(); sli != loadedStudies.end(); sli++)
        {
            // current patient study matches contain loaded study?
            StudyList::iterator li = std::find_if(currentStudies.begin(),
                                                  currentStudies.end(),
                                                  std::bind1st(std::mem_fun(&ent::Entity::comparePredicate), *sli)
                                                  );

            // loaded study was not found in current matches
            if (li == currentStudies.end())
            {
                // not found just add the loaded study
                currentStudies.push_back(*sli);
                this->loadSeriesStubs(*sli);
            }
            else
            {
                // check if loaded study can be updated
                ent::Study* currentStudy = *li;
                if (currentStudy->getStatus() == ent::STUB || currentStudy->getStatus() == ent::LOADED)
                {
                    currentStudy->update(*sli);
                }
            }
        }
    }

    return currentStudies;
}

ent::Study* Manager::getStudyById(int id, ent::EntityLoadingMode mode)
{
    avpp::StudyMap::iterator mi = this->studyMap.find(id);

    // not in cache
    if (mi == this->studyMap.end())
    {
        // load Study into cache
        QLOG_INFO() << "Manager::getStudyById - study (#" << id << ") not yet cached, loading...";
        ent::Study* study = this->getSpecificStudyDao()->getStudyById(id);

        if (study != nullptr)
        {
            study->setLoaded();
            this->studyMap[study->getId()] = study;
            QLOG_INFO() << "Manager::getStudyById - cached new study, loading stubs...";
            this->loadSeriesStubs(study);
            QLOG_INFO() << "Manager::getStudyById - finished loading series stubs, loading stubs...";
            this->loadPatientStub(study);
            QLOG_INFO() << "Manager::getStudyById - finished loading patient stub";
        }
        else
        {
            QLOG_WARN() << "Manager::getStudyById - failed to load new study";
        }

        return study;
    }
    else
    {
        // Study already in cache
        ent::Study* study = this->studyMap[id];
        if (study->getStatus() == ent::STUB)
        {
            QLOG_INFO() << "Manager::getStudyById - study (#" << id << ") is a stub, loading contents...";
            ent::Study* loadedStudy = this->getSpecificStudyDao()->getStudyById(id);

            if (loadedStudy != nullptr)
            {
                study->update(loadedStudy);
                study->setLoaded();
                QLOG_INFO() << "Manager::getStudyById - updated stubbed study, loading stubs...";
                this->loadSeriesStubs(study);
                QLOG_INFO() << "Manager::getStudyById - finished loading series stubs, loading stubs...";
                this->loadPatientStub(study);
                QLOG_INFO() << "Manager::getStudyById - finished loading patient stub";
            }
            else
            {
                QLOG_WARN() << "Manager::getStudyById - failed to update stubbed patient";
            }

            return study;
        }
        else
        {
            if (study->getStatus() == ent::DIRTY)
            {
                this->loadSeriesStubs(study);
                this->loadPatientStub(study);
                QLOG_INFO() << "Manager::getStudyById - Study " << study->getId() << " is dirty, loadSeriesStubs and loadPatientStub";
                return study;
            }
            else // study is ent::LOADED
            {
                if (mode == ent::EAGER)
                {
                    QLOG_INFO() << "Manager::getStudyById - updating study (#" << id << ") ...";
                    ent::Study* loadedStudy = this->getSpecificStudyDao()->getStudyById(id);

                    if (loadedStudy != nullptr)
                    {
                        study->update(loadedStudy);
                        QLOG_INFO() << "Manager::getStudyById - updated cached study, loading stubs...";
                        this->loadSeriesStubs(study);
                        QLOG_INFO() << "Manager::getStudyById - finished loading series stubs and stubs...";
                        this->loadPatientStub(study);
                        QLOG_INFO() << "Manager::getStudyById - finished loading patient stub";
                    }
                    else
                    {
                        QLOG_WARN() << "Manager::getStudyById - failed to update cached study";
                    }

                    return study;
                }
                else // mode == ent::LAZY
                {
                    return study;
                }
            }
        }

    }

    return nullptr;
}

ent::Study* Manager::getStudyByUid(QString uID)
{
	return this->getSpecificStudyDao()->getStudyByUid(uID);
}

ent::Study* Manager::getStudyBySeries(ent::Series* series, ent::EntityLoadingMode mode)
{
    if (series == nullptr)
    {
        QLOG_WARN() << "Manager::getStudyBySeries - passed series was nullptr";
        return nullptr;
    }

    // series already knows it's study?
    if (series->getStudy() != nullptr)
    {
        return this->getStudyById(series->getStudy()->getId());
    }
    else
    {
        ent::Study* study = this->loadStudyStub(series);

        if (mode == ent::EAGER && study != nullptr)
        {
            study->update(this->getStudyById(study->getId()));
        }

        return study;
    }
}

bool Manager::insertStudy(ent::Study* study)
{
    if (study == nullptr)
    {
        QLOG_WARN() << "Manager::insertStudy - passed study was nullptr";
        return false;
    }

    // study already cached?
    avpp::StudyMap::iterator smi = this->studyMap.find(study->getId());
    if (smi == this->studyMap.end())
    {
        /*
         * Load Study into cache and insert into database
         */
        QLOG_INFO() << "Manager::insertStudy - study (#" << study->getId() << ") not cached, inserting...";
        if (this->getSpecificStudyDao()->insertStudy(study))
        {
            // add study to local cache
            study->setLoaded();
            this->studyMap[study->getId()] = study;

            // add study to patient study matches
            if (study->getPatient() != nullptr)
            {
                StudyList sl = this->patientStudyMatches[study->getPatient()->getId()];
                sl.push_back(study);
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        /*
         * wenn er in der map ist und ich bekomm den paramtere als loaded hinein,
         * dann überreiche ich ihn ans update weiter
         */
        if (study->getStatus() == ent::LOADED)
        {
            // forward to update function
            return this->updateStudy(study);
        }
        // 1_WARNING revisit if dirty needs to be handled
    }

    return false;
}

bool Manager::updateStudy(ent::Study* study)
{
    if (study == nullptr)
    {
        QLOG_WARN() << "Manager::updateStudy - passed study was nullptr";
        return false;
    }

    // study already in cache and not dirty
    StudyMap::iterator smi = this->studyMap.find(study->getId());
    if (smi != this->studyMap.end())
    {
        ent::Study* cachedStudy = this->studyMap[study->getId()];
        // if study is not dirty
        if (cachedStudy->getStatus() != ent::DIRTY &&
                this->getSpecificStudyDao()->updateStudy(study))
        {
            cachedStudy->setLoaded();
            return true;
        }
    }

    return false;
}

bool Manager::deleteStudy(ent::Study* study)
{
    if (study == nullptr)
    {
        QLOG_WARN() << "Manager::deleteStudy - passed study was nullptr";
        return false;
    }

    // study has matches to series?
    SeriesList sl = this->studySeriesMatches[study->getId()];
    if (sl.size() == 0)
    {
        this->studySeriesMatches.erase(study->getId());

        // acutally delete the study
        if (this->getSpecificStudyDao()->deleteStudy(study))
        {
            this->studyMap.erase(study->getId());
            return true;
        }
    }

    return false;
}

SeriesList Manager::getAllSeriesByStudy(ent::Study* study)
{
    if (study == nullptr)
    {
        QLOG_WARN() << "Manager::getAllSeriesByStudy - passed study was nullptr";
        return SeriesList();
    }

    SeriesList loadedSeries = this->getSpecificSeriesDao()->getAllSeriesByStudy(study);

    // series already cached in matches
    SeriesList currentSeries = this->studySeriesMatches[study->getId()];
    if (currentSeries.size() == 0)
    {
        // not cached yet just write loadedSeries list directly into matches and preload series stubs
        this->studySeriesMatches.erase(study->getId());
        currentSeries = loadedSeries;
        this->studySeriesMatches[study->getId()] = currentSeries;

        for (SeriesList::const_iterator sli = currentSeries.begin(); sli != currentSeries.end(); sli++)
        {
            this->loadFileStubs(*sli);
        }
    }
    else
    {
        for (SeriesList::const_iterator sli = loadedSeries.begin(); sli != loadedSeries.end(); sli++)
        {
            // current study series matches contain loaded series?
            SeriesList::iterator li = std::find_if(currentSeries.begin(),
                                                  currentSeries.end(),
                                                  std::bind1st(std::mem_fun(&ent::Entity::comparePredicate), *sli)
                                                  );

            // loaded series was found in current matches
            if (li == currentSeries.end())
            {
                // not found just add the loaded series
                currentSeries.push_back(*sli);
                this->loadFileStubs(*sli);
            }
            else
            {
                // check if loaded series can be updated
                ent::Series* currentSeries = *li;
                if (currentSeries->getStatus() == ent::STUB || currentSeries->getStatus() == ent::LOADED)
                {
                    currentSeries->update(*sli);
                }
            }
        }
    }

    return currentSeries;
}

ent::Series* Manager::getSeriesById(int id, ent::EntityLoadingMode mode)
{
    avpp::SeriesMap::iterator mi = this->seriesMap.find(id);

    // not in cache
    if (mi == this->seriesMap.end())
    {
        // load Series into cache
        QLOG_INFO() << "Manager::getSeriesById - series (#" << id << ") not yet cached, loading...";
        ent::Series* series = this->getSpecificSeriesDao()->getSeriesById(id);

        if (series != nullptr)
        {
            series->setLoaded();
            this->seriesMap[series->getId()] = series;
            QLOG_INFO() << "Manager::getSeriesById - cached new series, loading stubs...";
            this->loadFileStubs(series);
            QLOG_INFO() << "Manager::getSeriesById - finished loading file stubs, loading stubs...";
            this->loadStudyStub(series);
            QLOG_INFO() << "Manager::getSeriesById - finished loading study stub";
        }
        else
        {
            QLOG_WARN() << "Manager::getSeriesById - failed to load new series";
        }

        return series;
    }
    else
    {
        // Series already in cache
        ent::Series* series = this->seriesMap[id];
        if (series->getStatus() == ent::STUB)
        {
            QLOG_INFO() << "Manager::getSeriesById - series (#" << id << ") is a stub, loading contents...";
            ent::Series* loadedSeries = this->getSpecificSeriesDao()->getSeriesById(id);

            if (loadedSeries != nullptr)
            {
                series->update(loadedSeries);
                series->setLoaded();
                QLOG_INFO() << "Manager::getSeriesById - updated stubbed series, loading stubs...";
                this->loadFileStubs(series);
                QLOG_INFO() << "Manager::getSeriesById - finished loading file stubs, loading stubs...";
                this->loadStudyStub(series);
                QLOG_INFO() << "Manager::getSeriesById - finished loading study stub";
            }
            else
            {
                QLOG_WARN() << "Manager::getSeriesById - failed to update stubbed series";
            }

            return series;
        }
        else
        {
            if (series->getStatus() == ent::DIRTY)
            {
                this->loadFileStubs(series);
                this->loadStudyStub(series);
                QLOG_INFO() << "Manager::getSeriesById - Series " << series->getId() << " is dirty, loadFileStubs and loadStudyStub";
                return series;
            }
            else // study is ent::LOADED
            {
                if (mode == ent::EAGER)
                {
                    QLOG_INFO() << "Manager::getSeriesById - updating study (#" << id << ") ...";
                    ent::Series* loadedSeries = this->getSpecificSeriesDao()->getSeriesById(id);

                    if (loadedSeries != nullptr)
                    {
                        series->update(loadedSeries);
                        QLOG_INFO() << "Manager::getSeriesById - updated cached series, loading stubs...";
                        this->loadFileStubs(series);
                        QLOG_INFO() << "Manager::getSeriesById - finished loading file stubs and stubs...";
                        this->loadStudyStub(series);
                        QLOG_INFO() << "Manager::getSeriesById - finished loading study stub";
                    }
                    else
                    {
                        QLOG_WARN() << "Manager::getSeriesById - failed to update cached study";
                    }

                    return series;
                }
                else // mode == ent::LAZY
                {
                    return series;
                }
            }
        }

    }

    return nullptr;
}

ent::Series* Manager::getSeriesByUid(QString uID)
{
	return this->getSpecificSeriesDao()->getSeriesByUid(uID);
}

ent::Series* Manager::getSeriesByFile(ent::File* file, ent::EntityLoadingMode mode)
{
    if (file == nullptr)
    {
        QLOG_WARN() << "Manager::getSeriesByFile - passed file was nullptr";
        return nullptr;
    }

    // file already knows it's series?
    if (file->getSeries() != nullptr)
    {
        return this->getSeriesById(file->getSeries()->getId());
    }
    else
    {
        ent::Series* series = this->loadSeriesStub(file);

        if (mode == ent::EAGER && series != nullptr)
        {
            series->update(this->getSeriesById(series->getId()));
        }

        return series;
    }
}

bool Manager::insertSeries(ent::Series* series)
{
    if (series == nullptr)
    {
        QLOG_WARN() << "Manager::insertSeries - passed series was nullptr";
        return false;
    }

    // series already cached?
    avpp::SeriesMap::iterator smi = this->seriesMap.find(series->getId());
    if (smi == this->seriesMap.end())
    {
        /*
         * Load series into cache and insert into database
         */
        QLOG_INFO() << "Manager::insertSeries - series (#" << series->getId() << ") not cached, inserting...";
        if (this->getSpecificSeriesDao()->insertSeries(series))
        {
            // insert series to local cache
            series->setLoaded();
            this->seriesMap[series->getId()] = series;

            // add series to study series matches
            if (series->getStudy() != nullptr)
            {
                SeriesList sl = this->studySeriesMatches[series->getStudy()->getId()];
                sl.push_back(series);
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        /*
         * wenn er in der map ist und ich bekomm den paramtere als loaded hinein,
         * dann überreiche ich ihn ans update weiter
         */
        if (series->getStatus() == ent::LOADED)
        {
            // forward to update function
            return this->updateSeries(series);
        }
        // 1_WARNING revisit if dirty needs to be handled
    }

    return false;
}

bool Manager::updateSeries(ent::Series* series)
{
    if (series == nullptr)
    {
        QLOG_WARN() << "Manager::updateSeries - passed series was nullptr";
        return false;
    }

    // series already in cache and not dirty
    SeriesMap::iterator smi = this->seriesMap.find(series->getId());
    if (smi != this->seriesMap.end())
    {
        ent::Series* cachedSeries = this->seriesMap[series->getId()];
        // if series is not dirty
        if (cachedSeries->getStatus() != ent::DIRTY &&
                this->getSpecificSeriesDao()->updateSeries(series))
        {
            cachedSeries->setLoaded();
            return true;
        }
    }

    return false;
}

bool Manager::deleteSeries(ent::Series* series)
{
    if (series == nullptr)
    {
        QLOG_WARN() << "Manager::deleteSeries - passed series was nullptr";
        return false;
    }

    // series has matches to files?
    FileList sl = this->seriesFileMatches[series->getId()];
    if (sl.size() == 0)
    {
        this->seriesFileMatches.erase(series->getId());

        // acutally delete the series
        if (this->getSpecificSeriesDao()->deleteSeries(series))
        {
            this->seriesMap.erase(series->getId());
            return true;
        }
    }

    return false;
}

FileList Manager::getAllFiles()
{
    FileList temporaryFileList = this->getSpecificFileDao()->getAllFiles();

    /*
     * iterate over all patients to update local cache and so on
     */
    for (avpp::FileList::iterator ci = temporaryFileList.begin(); ci != temporaryFileList.end(); ci++)
    {
        ent::File* newFile = *ci;

        /*
         * does local file cache alread contain file?
         */
        avpp::FileMap::const_iterator mi = this->fileMap.find(newFile->getId());
        if (mi == this->fileMap.end())
        {
            /*
             * file currently not cached, add + load dependent stubs
             */
            QLOG_INFO() << "Manager::getAllFiles - file (#" << newFile->getId() << ") not yet cached, loading into cache";
            this->fileMap[newFile->getId()] = newFile;
            //QLOG_INFO() << "Manager::getAllFiles - loading stubs for file (#" << newFile->getId() << ")...";
            //this->loadSeriesStub(newFile);
        }
        else
        {
            /*
             * file already in cache, retrieve current version
             */
            ent::File* currentlyLoadedFile = this->fileMap[newFile->getId()];

            if (currentlyLoadedFile->getStatus() == ent::STUB)
            {
                /*
                 * currently cached file was only a stub, update (not break pointer
                 * integrity here) with loaded file and load dependent stubs
                 */
                QLOG_INFO() << "Manager::getAllFiles - cached file (#" << currentlyLoadedFile->getId() << ") was only a stub, updating...";
                ent::File* stubbedFile = this->fileMap[currentlyLoadedFile->getId()];
                stubbedFile->update(newFile);
                //QLOG_INFO() << "Manager::getAllFiles - loading series stubs for file...";
                //this->loadSeriesStub(stubbedFile);
            }
            else
            {
                QLOG_INFO() << "Manager::getAllFiles - already cached file (#" << newFile->getId() << ") loaded";
                /*
                 * check if current file is dirty or loaded
                 */
                if (currentlyLoadedFile->getStatus() == ent::DIRTY)
                {
                    /*
                     * discard loaded version to prevent loss of dirty data
                     * assumption, stubs were already loaded
                     */
                    QLOG_WARN() << "Manager::getAllFiles - discareded file (#" << newFile->getId() << ") local version is dirty";
                    *ci = currentlyLoadedFile;
                }
                else
                {
                    /*
                     * update currently loaded file (don't break pointer integrity here)
                     * with updated values check if stubs were already loaded and load them
                     * if not
                     */
                    ent::File* loadedFile = this->fileMap[currentlyLoadedFile->getId()];
                    loadedFile->update(newFile);
                    /*
                    SeriesFileMap::const_iterator fileMatches = this->seriesFileMatches.find(loadedFile->getId());
                    if (fileMatches == this->seriesFileMatches.end())
                    {
                        this->loadSeriesStub(loadedFile);
                    }
                    else
                    {
                        QLOG_INFO() << "Manager::getAllFiles - series stubs for file (#" << loadedFile->getId() << ") already loaded";
                    }
                    */
                }
            }
        }
    }

    return temporaryFileList;

}

FileList Manager::getAllFilesByPatient(ent::Patient* patient)
{
    if (patient == nullptr)
    {
        QLOG_WARN() << "Manager::getAllFilesByPatient - passed patient was nullptr";
        return FileList();
    }

    FileList loadedFiles = this->getSpecificFileDao()->getAllFilesByPatient(patient);

    for (FileList::const_iterator li = loadedFiles.begin(); li != loadedFiles.end(); li++)
    {
        ent::File* file = *li;
        ent::Series* series = file->getSeries();

        // files already cached in matches
        FileList currentFiles = this->seriesFileMatches[series->getId()];
        if (currentFiles.size() == 0)
        {
            // not cached yet just write loadedFiles list directly into matches and preload series stubs
            this->seriesFileMatches.erase(series->getId());
            currentFiles = loadedFiles;
            this->seriesFileMatches[series->getId()] = currentFiles;
        }
        else
        {
            for (FileList::const_iterator fli = loadedFiles.begin(); fli != loadedFiles.end(); fli++)
            {
                // current file series matches contain loaded series?
                FileList::iterator li = std::find_if(currentFiles.begin(),
                                                     currentFiles.end(),
                                                     std::bind1st(std::mem_fun(&ent::Entity::comparePredicate), *fli)
                                                     );

                // loaded file was found in current matches
                if (li == currentFiles.end())
                {
                    // not found just add the loaded file
                    currentFiles.push_back(*fli);
                    //this->loadSeriesStub(*fli);
                }
                else
                {
                    // check if loaded file can be updated
                    ent::File* currentFile = *li;
                    if (currentFile->getStatus() == ent::STUB || currentFile->getStatus() == ent::LOADED)
                    {
                        currentFile->update(*fli);
                    }
                }
            }
        }
    }

    return loadedFiles;
}


FileList Manager::getAllFilesBySeries(ent::Series* series)
{
    if (series == nullptr)
    {
        QLOG_WARN() << "Manager::getAllFilesBySeries - passed series was nullptr";
        return FileList();
    }

    FileList loadedFiles = this->getSpecificFileDao()->getAllFilesBySeries(series);

    // files already cached in matches
    FileList currentFiles = this->seriesFileMatches[series->getId()];
    if (currentFiles.size() == 0)
    {
        // not cached yet just write loadedFiles list directly into matches and preload series stubs
        this->seriesFileMatches.erase(series->getId());
        currentFiles = loadedFiles;
        this->seriesFileMatches[series->getId()] = currentFiles;
    }
    else
    {
        for (FileList::const_iterator fli = loadedFiles.begin(); fli != loadedFiles.end(); fli++)
        {
            // current file series matches contain loaded series?
            FileList::iterator li = std::find_if(currentFiles.begin(),
                                                  currentFiles.end(),
                                                  std::bind1st(std::mem_fun(&ent::Entity::comparePredicate), *fli)
                                                  );

            // loaded file was found in current matches
            if (li == currentFiles.end())
            {
                // not found just add the loaded file
                currentFiles.push_back(*fli);
                this->loadSeriesStub(*fli);
            }
            else
            {
                // check if loaded file can be updated
                ent::File* currentFile = *li;
                if (currentFile->getStatus() == ent::STUB || currentFile->getStatus() == ent::LOADED)
                {
                    currentFile->update(*fli);
                }
            }
        }
    }

    return currentFiles;
}

ent::File* Manager::getFileById(int id, ent::EntityLoadingMode mode)
{
    avpp::FileMap::iterator mi = this->fileMap.find(id);

    // not in cache
    if (mi == this->fileMap.end())
    {
        // load File into cache
        QLOG_INFO() << "Manager::getFileById - file (#" << id << ") not yet cached, loading...";
        ent::File* file = this->getSpecificFileDao()->getFileById(id);

        if (file != nullptr)
        {
            file->setLoaded();
            this->fileMap[file->getId()] = file;
            QLOG_INFO() << "Manager::getFileById - caching file, loading stubs...";
            this->loadSeriesStub(file);
            QLOG_INFO() << "Manager::getFileById - finished loading file and stubs";
        }
        else
        {
            QLOG_WARN() << "Manager::getFileById - failed to load file";
        }

        return file;
    }
    else
    {
        // File already in cache
        ent::File* file = this->fileMap[id];
        if (file->getStatus() == ent::STUB)
        {
            QLOG_INFO() << "Manager::getFilebyId - file (#" << id << ") is a stub, loading contents...";
            ent::File* loadedFile = this->getSpecificFileDao()->getFileById(id);

            if (loadedFile != nullptr)
            {
                file->update(loadedFile);
                file->setLoaded();
                QLOG_INFO() << "Manager::getFilebyId - updated stubbed file, loading stubs...";
                this->loadSeriesStub(file);
                QLOG_INFO() << "Manager::getFilebyId - finished loading series stub";
            }
            else
            {
                QLOG_WARN() << "Manager::getFilebyId - failed to update stubbed file";
            }

            return file;
        }
        else
        {
            if (file->getStatus() == ent::DIRTY)
            {
                this->loadSeriesStub(file);
                QLOG_INFO() << "Manager::getFileById - file " << file->getId() << " is dirty, loadSeriesStub";
                return file;
            }
            else // file is ent::LOADED
            {
                if (mode == ent::EAGER)
                {
                    QLOG_INFO() << "Manager::getFileById - updating file (#" << id << ") ...";
                    ent::File* loadedFile = this->getSpecificFileDao()->getFileById(id);

                    if (loadedFile != nullptr)
                    {
                        file->update(loadedFile);
                        QLOG_INFO() << "Manager::getFileById - updated cached file, loading stubs...";
                        this->loadSeriesStub(file);
                        QLOG_INFO() << "Manager::getFileById - finished loading series stub";
                    }
                    else
                    {
                        QLOG_WARN() << "Manager::getFileById - failed to update cached file";
                    }

                    return file;
                }
                else // mode == ent::LAZY
                {
                    return file;
                }
            }
        }
    }

    return nullptr;

}

ent::File* Manager::getFileByName(QString name, ent::EntityLoadingMode mode)
{
    ent::File* file = nullptr;

    // iterate over filemap to find file by name
    for (avpp::FileMap::iterator mi = this->fileMap.begin(); mi != this->fileMap.end(); ++mi)
    {
        ent::File* currFile = mi->second;
        if (currFile->getName() == name)
        {
            file = currFile;
            break;
        }
    }

    // not in cache
    if (file == nullptr)
    {
        QLOG_INFO() << "Manager::getFileByName - file (name: " << name << ") not yet cached, loading...";
        file = this->getSpecificFileDao()->getFileByName(name);

        if (file != nullptr)
        {
            file->setLoaded();
            this->fileMap[file->getId()] = file;
            QLOG_INFO() << "Manager::getFileByName - caching file, loading stubs... ";
            this->loadSeriesStub(file);
            QLOG_INFO() << "Manager::getFileByName - gfinished loading file and stubs";
        }
        else
        {
            QLOG_WARN() << "Manager::getFileByName - failed to laod file";
        }

        return file;
    }
    else
    {
        if (file->getStatus() == ent::STUB)
        {
            QLOG_INFO() << "Manager::getFileByName - file (name: " << name << ") is a stub, loading contents...";
            ent::File* loadedFile = this->getSpecificFileDao()->getFileByName(name);

            if (loadedFile != nullptr)
            {
                file->update(loadedFile);
                file->setLoaded();
                QLOG_INFO() << "Manager::getFileByName - updated stubbed file, loading stubs...";
                this->loadSeriesStub(file);
                QLOG_INFO() << "Manager::getFileByName - finished loading series stub";
            }
            else
            {
                QLOG_WARN() << "Manager::getFileByName - failed to update stubbed file";
            }

            return file;
        }
        else
        {
            if (file->getStatus() == ent::DIRTY)
            {
                this->loadSeriesStub(file);
                QLOG_INFO() << "Manager::getFileByName - file " << file->getId() << " is dirty, loadSeriesStub";
                return file;
            }
            else // file is ent::LOADED
            {
                if (mode == ent::EAGER)
                {
                    QLOG_INFO() << "Manager::getFileByName - updating file (name: " << name << ") ...";
                    ent::File* loadedFile = this->getSpecificFileDao()->getFileByName(name);

                    if (loadedFile != nullptr)
                    {
                        file->update(loadedFile);
                        QLOG_INFO() << "Manager::getFileByName - updated cached file, loading stubs...";
                        this->loadSeriesStub(file);
                        QLOG_INFO() << "Manager::getFileByName - finished loading series stub";
                    }
                    else
                    {
                        QLOG_WARN() << "Manager::getFileByName - failed to update cached file";
                    }

                    return file;
                }
                else // mode == ent::LAZY
                {
                    return file;
                }
            }
        }
    }

    return file;
}

bool Manager::insertFile(ent::File* file)
{
    if (file == nullptr)
    {
        QLOG_WARN() << "Manager::insertFile - passed file was nullptr";
        return false;
    }

    // File already cached?
    avpp::FileMap::iterator mi = this->fileMap.find(file->getId());
    if (mi == this->fileMap.end())
    {
        /*
         * Load File into cache and insert into database
         */
        QLOG_INFO() << "Manager::insertFile - file (#" << file->getId() << ") not cached, inserting...";
        if (this->getSpecificFileDao()->insertFile(file))
        {
            // insert file to local cache
            this->fileMap[file->getId()] = file;

            // add file to series file matches
            if (file->getSeries() != nullptr)
            {
                FileList fl = this->seriesFileMatches[file->getSeries()->getId()];
                fl.push_back(file);
            }

            bool success = file->persistBinaryData();

            if (!success)
            {
                QLOG_ERROR() << "Manager::insertFile - failed to save binary data to storage";
            }

            return success;
        }
        else
        {
            return false;
        }
    }
    else
    {
        /*
         * wenn er in der map ist und ich bekomm den paramtere als loaded hinein,
         * dann überreiche ich ihn ans update weiter
         */
        if (file->getStatus() == ent::LOADED)
        {
            // forward to update function
            return this->updateFile(file);
        }
        // 1_WARNING revisit if dirty needs to be handled
    }

    return false;
}

bool Manager::updateFile(ent::File* file)
{
    if (file == nullptr)
    {
        QLOG_WARN() << "Manager::updateFile - passed file was nullptr";
        return false;
    }

    // File already in cache and not dirty
    FileMap::iterator fmi = this->fileMap.find(file->getId());
    if (fmi != this->fileMap.end())
    {
        ent::File* cachedFile = this->fileMap[file->getId()];
        // if file is not dirty
        if (cachedFile->getStatus() != ent::DIRTY &&
                this->getSpecificFileDao()->updateFile(file))
        {
            cachedFile->setBinaryData(file->getBinaryData(), file->getSize());
            bool success = cachedFile->persistBinaryData();

            if (!success)
            {
                QLOG_ERROR() << "Manager::updateFile - failed to save binary data to storage";
            }

            return success;
        }
    }

    return false;
}

bool Manager::deleteFile(ent::File* file)
{
    if (file == nullptr)
    {
        QLOG_WARN() << "Manager::deleteFile - passed file was nullptr";
        return false;
    }

    // File in FileMap?
    FileMap::iterator fmi = this->fileMap.find(file->getId());
    if (fmi != this->fileMap.end())
    {
        // File sucessfully deleted?
        if (this->getSpecificFileDao()->deleteFile(file))
        {
            // Erase File from Map and Matches
            this->fileMap.erase(file->getId());
            this->seriesFileMatches.erase(file->getSeries()->getId());

            bool success = file->eraseBinaryData();

            if (!success)
            {
                QLOG_ERROR() << "Manager::deleteFile - failed to erase binary data to storage";
            }

            return success;
        }
    }

    return false;
}

FileTypeList Manager::getAllFileTypes()
{
    return this->fileTypeList;
}

ent::FileType* Manager::getFileTypeById(int id)
{
    return this->fileTypeMap[id];
}

ent::FileType* Manager::getFileTypeByName(QString name)
{
	FileTypeList l = this->getAllFileTypes();
	for (FileTypeList::const_iterator iter = l.begin(); iter != l.end(); ++iter)
	{
		ent::FileType* ft = *iter;
		if (ft->getName() == name)
		{
			return ft;
		}
	}

	return nullptr;
}

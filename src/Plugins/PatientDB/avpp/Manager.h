#pragma once

#include "Avpp.h"
#include "Connection.h"

namespace avpp
{
    namespace dao
    {
        class PatientDao;
        class StudyDao;
        class SeriesDao;
        class FileDao;
        class FileTypeDao;
    }

    class Manager
    {
        friend class ent::Patient;
        friend class ent::Study;
        friend class ent::Series;
        friend class ent::File;
        friend class ent::FileType;

    public:
        static Manager* getInstance();
        static void cleanInstance();

    private:
        Manager();
        Manager(DAO_TYPE daoType);
        Manager(Manager const& copy);
        ~Manager();

        void operator=(Manager const& copy);
        void init(DAO_TYPE daoType);
        bool isReady();

        PatientMap patientMap;
        StudyMap studyMap;
        SeriesMap seriesMap;
        FileMap fileMap;
        FileTypeMap fileTypeMap;
        FileTypeList fileTypeList;

        PatientStudyMap patientStudyMatches;
        StudySeriesMap studySeriesMatches;
        SeriesFileMap seriesFileMatches;

        static Manager* instance;
        Connection* connection;
        DAO_TYPE daoType;

        dao::PatientDao* patientDao;
        dao::StudyDao* studyDao;
        dao::SeriesDao* seriesDao;
        dao::FileDao* fileDao;
        dao::FileTypeDao* fileTypeDao;

        void assignNewSpecificPatientDao();
        dao::PatientDao* getSpecificPatientDao();
        void assignNewSpecificStudyDao();
        dao::StudyDao* getSpecificStudyDao();
        void assignNewSpecificSeriesDao();
        dao::SeriesDao* getSpecificSeriesDao();
        void assignNewSpecificFileDao();
        dao::FileDao* getSpecificFileDao();
        void assignNewSpecificFileTypeDao();
        dao::FileTypeDao* getSpecificFileTypeDao();

        ent::Patient* loadPatientStub(ent::Study* study);
        void loadStudyStubs(ent::Patient* patient);
        ent::Study* loadStudyStub(ent::Series* series);
        void loadSeriesStubs(ent::Study* study);
        ent::Series* loadSeriesStub(ent::File* file);
        void loadFileStubs(ent::Series* series);

    public:
        /*
         * Patient functions
         */
        PatientList getAllPatients();
        ent::Patient* getPatientById(int id, ent::EntityLoadingMode mode = ent::EAGER);
		ent::Patient* getPatientByPid(QString pID);
        ent::Patient* getPatientByStudy(ent::Study* study, ent::EntityLoadingMode mode = ent::EAGER);
        bool insertPatient(ent::Patient* patient);
        bool updatePatient(ent::Patient* patient);
        bool deletePatient(ent::Patient* patient);

        /*
         * Study functions
         */
        StudyList getAllStudiesByPatient(ent::Patient* patient);
        ent::Study* getStudyById(int id, ent::EntityLoadingMode mode = ent::EAGER);
		ent::Study* getStudyByUid(QString uID);
        ent::Study* getStudyBySeries(ent::Series* series, ent::EntityLoadingMode mode = ent::EAGER);
        bool insertStudy(ent::Study* study);
        bool updateStudy(ent::Study* study);
        bool deleteStudy(ent::Study* study);

        /*
         * Series functions
         */
        SeriesList getAllSeriesByStudy(ent::Study* study);
        ent::Series* getSeriesById(int id, ent::EntityLoadingMode mode = ent::EAGER);
        ent::Series* getSeriesByUid(QString uID);
        ent::Series* getSeriesByFile(ent::File* file, ent::EntityLoadingMode mode = ent::EAGER);
        bool insertSeries(ent::Series* series);
        bool updateSeries(ent::Series* series);
        bool deleteSeries(ent::Series* series);

        /*
         * File functions
         */
		FileList getAllFiles();
        FileList getAllFilesByPatient(ent::Patient* patient);
        FileList getAllFilesBySeries(ent::Series* series);
        ent::File* getFileById(int id, ent::EntityLoadingMode mode = ent::EAGER);
        ent::File* getFileByName(QString name, ent::EntityLoadingMode mode = ent::EAGER);
        bool insertFile(ent::File* file);
        bool updateFile(ent::File* file);
        bool deleteFile(ent::File* file);

        /*
         * FileType functions
         */
        FileTypeList getAllFileTypes();
		ent::FileType* getFileTypeById(int id);
		ent::FileType* getFileTypeByName(QString name);
    };
}

#include "FileManager.h"
#include "../ent/File.h"
#include "../util/Settings.h"
#include <QCryptographicHash>
#include <QDir>
#include <fstream>
#include "util/qslog/QsLog.h"

using namespace avpp;
using namespace avpp::bin;

FileManager* FileManager::instance = nullptr;

FileManager::FileManager()
{
    QLOG_INFO() << "initializing FileManager...";

    util::Settings settings(CONFIG_PATH);
    bool settingsOk = settings.load();

    QDir dir;

    /*
     * read following settings from config file:
     * - storagePath
     * - trashPath
     */
    if (settingsOk)
    {
        QVariantMap map = settings.getResult().toMap()["Storage"].toMap();

        this->storagePath = map["path"].toString();
        if (this->storagePath == "")
        {
            QLOG_WARN() << "FileManager::FileManager - config file did not specify storage path";
            this->storagePath = QDir::currentPath() + "files" + QDir::separator();
            QLOG_INFO() << "FileManager::FileManager - falling back to " << this->storagePath;
        }
        else
        {
            QLOG_INFO() << "FileManager::FileManager - read storagePath: " << this->storagePath;
        }

        this->trashPath = map["trash"].toString();
        if (this->trashPath == "")
        {
            QLOG_WARN() << "FileManager::FileManager - config file did not specify trash path";
            this->trashPath = this->storagePath + ".Trash" + QDir::separator();
            QLOG_INFO() << "FileManager::FileManager - falling back to " << this->trashPath;
        }
        else
        {
            QLOG_INFO() << "FileManager::FileManager - read trashPath: " << this->trashPath;
        }
    }
    else
    {
        QLOG_ERROR() << "FileManager::FileManager - could not read settings from json config file";
        this->storagePath = QDir::currentPath() + "files" + QDir::separator();
        QLOG_INFO() << "FileManager::FileManager - falling back to storagePath: " << this->storagePath;
        this->trashPath = this->storagePath + ".Trash" + QDir::separator();
        QLOG_INFO() << "FileManager::FileManager - falling back to trashPath: " << this->trashPath;
    }

    QLOG_INFO() << "FileManager::FileManager - creating full path (" << this->storagePath << "): " << dir.mkpath(this->storagePath);
    QLOG_INFO() << "FileManager::FileManager - creating full path (" << this->trashPath << "): " << dir.mkpath(this->trashPath);
}

FileManager::~FileManager()
{

}

FileManager* FileManager::getInstance()
{
    if (FileManager::instance == nullptr)
    {
        FileManager::instance = new FileManager();
    }

    return FileManager::instance;
}

char* FileManager::getFile(ent::File &fileEnt, bool readingFileFirstTime)
{
    QString filePath;
    if (readingFileFirstTime)
    {
        filePath = fileEnt.getName();
        fileEnt.setName(fileEnt.getName().section("/", -1));
    }
    else
    {
         filePath = this->storagePath + this->obfuscateFileName(fileEnt.getName());
    }

    char* data;
    int size;

    std::ifstream fileIn(filePath.toStdString(), std::ifstream::in|std::ifstream::binary|std::ifstream::ate);

    if (fileIn.is_open())
    {
        size = fileIn.tellg();
        fileIn.seekg(0, fileIn.beg);

        data = new char[size];

        QLOG_INFO() << "FileManager::getFile - reading " << size << "bytes...";
        fileIn.read(data, size);

        if (fileIn)
        {
            QLOG_INFO() << "FileManager::getFile - read data successfully";
            fileIn.close();
        }
        else
        {
            QLOG_ERROR() << "FileManager::getFile - failed to load data";
            fileIn.close();
            return nullptr;
        }
    }
    else
    {
        QLOG_ERROR() << "FileManager::getFile - failed to open ifstream on file (" << filePath << ")";
        return nullptr;
    }

    QByteArray checksum = this->computeHash(data, size);

    if (readingFileFirstTime)
    {
        fileEnt.setBinaryData(data, size);
        fileEnt.size = size;
        fileEnt.checksum = checksum;
        fileEnt.setDirty();

        return data;
    }
    else
    {
        if (checksum == fileEnt.getChecksum())
        {
            QLOG_INFO() << "FileManager::getFile - successfully read file (" << filePath << ") with checksum (" << QString(checksum.toBase64()) << ")";
            return data;
        }

        if (fileEnt.getSize() != size)
        {
            QLOG_WARN() << "FileManager::getFile - current file size (" << fileEnt.size << ") was different from file size read (" << size << ")";
            fileEnt.size = size;
            fileEnt.setDirty();
        }
    }

    QLOG_ERROR() << "FileManager::getFile - requested file (" << filePath << ") could not be read";
    return nullptr;
}

bool FileManager::persistFile(ent::File &fileEnt)
{
    if (fileEnt.getStatus() != ent::DIRTY)
    {
        QLOG_WARN() << "FileManager::persistFile - passed file was not dirty";
        return false;
    }

    QString fileName = this->obfuscateFileName(fileEnt.getName());
    QString filePath = this->storagePath + fileName;

    QLOG_DEBUG() << "FileManager::persistFile - persisting" << fileName << "@" << filePath;

    this->eraseFile(filePath, fileName);

    std::ofstream fileOut(filePath.toStdString(), std::ofstream::out|std::ofstream::binary);

    if (!fileOut.is_open())
    {
        QLOG_ERROR() << "FileManager::persistFile - could not open file (" << filePath << ") for writing";
        return false;
    }

    QLOG_INFO() << "FileManager::persistFile - writing" << fileEnt.getSize() << "bytes...";
    fileOut.write(fileEnt.getBinaryData(), fileEnt.getSize());

    return true;
}

bool FileManager::eraseFile(ent::File &fileEnt)
{
    QString fileName = this->obfuscateFileName(fileEnt.getName());
    QString filePath = this->storagePath + fileName;

    return this->eraseFile(filePath, fileName);
}

bool FileManager::eraseFile(QString filePath, QString fileName)
{
    // move file to .Trash if already stored
    QFile file(filePath);
    if (file.exists())
    {
        // 2_TODO file history rotation
        QFile::remove(this->trashPath + fileName);

        bool success = file.rename(this->trashPath + fileName);
        QLOG_INFO() << "FileManager::eraseFile - moved file (" << filePath << ") to (" << this->trashPath << fileName << "):" << success;
        return true;
    }

    return false;
}

QString FileManager::obfuscateFileName(QString fileName)
{
    QString obfuscated = QString(this->computeHash(fileName).toBase64());
    obfuscated.replace('/', '_');
    obfuscated.replace('+', '-');

    return obfuscated;
}

QByteArray FileManager::computeHash(char* file, int size)
{
    QCryptographicHash crypto(QCryptographicHash::Md5);

    int chunkSize = 8192;
    char* buf = nullptr;

    while (size > chunkSize)
    {
        buf = new char[chunkSize];
        memset(buf, 0, chunkSize);
        memcpy(buf, file, chunkSize);
        crypto.addData(buf, chunkSize);
        delete[] buf;
        file += chunkSize;
        size -= chunkSize;
    }

    crypto.addData(file, size);

    return crypto.result();
}

QByteArray FileManager::computeHash(QString string)
{
    QByteArray stringData;
    stringData.append(string);
    return QCryptographicHash::hash(stringData,QCryptographicHash::Md5);
}

#pragma once

#include "Avpp.h"
#include <QFile>

namespace avpp
{
    namespace ent
    {
        class File;
    }

    namespace bin
    {

        class FileManager
        {
            friend class ent::File;

        public:
            static FileManager* getInstance();

        private:
            FileManager();
            FileManager(FileManager const& copy);
            ~FileManager();

            void operator=(FileManager const& copy);

			char* getFile(ent::File &fileEnt, bool readingFileFirstTime = false);
            bool persistFile(ent::File &fileEnt);
            bool eraseFile(QString filePath, QString fileName);
            bool eraseFile(ent::File &fileEnt);

			QString obfuscateFileName(QString fileName);

            QByteArray computeHash(char* file, int size);
            QByteArray computeHash(QString string);

            static FileManager* instance;
            QString storagePath;
            QString trashPath;
        };
    }
}

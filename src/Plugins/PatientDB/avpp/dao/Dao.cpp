#include "Dao.h"

using namespace avpp;
using namespace avpp::dao;

Dao::Dao(Manager *manager)
{
    this->manager = manager;
}

Manager* Dao::getManager()
{
    return this->manager;
}

#pragma once

#include "../Avpp.h"

namespace avpp
{
    class Manager;

    namespace dao
    {
        class Dao
        {
        public:
            Dao(Manager* manager);
            virtual ~Dao() {}

            Manager* getManager();

            virtual avpp::DAO_TYPE getDaoType() = 0;

        private:
            Manager* manager;
        };
    }
}

#pragma once

#include "../Avpp.h"
#include "Dao.h"

namespace avpp
{
    class Manager;

    namespace dao
    {
        class PatientDao : public Dao
        {
        public:
            PatientDao(Manager* manager);
            virtual ~PatientDao() {}

            virtual PatientList getAllPatients() = 0;
            virtual avpp::ent::Patient* getPatientStubByStudy(avpp::ent::Study* study) = 0;
            virtual avpp::ent::Patient* getPatientById(int id) = 0;
            virtual avpp::ent::Patient* getPatientByPid(QString pID) = 0;
            virtual bool insertPatient(avpp::ent::Patient* patient) = 0;
            virtual bool updatePatient(avpp::ent::Patient* patient) = 0;
            virtual bool deletePatient(avpp::ent::Patient* patient) = 0;
        };
    }
}

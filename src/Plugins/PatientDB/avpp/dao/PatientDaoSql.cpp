#include "PatientDaoSql.h"
#include "../ent/Patient.h"
#include "../ent/Study.h"
#include "../util/sqlhelper.h"

#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlError>

using namespace avpp;
using namespace avpp::dao;
using namespace avpp::ent;

PatientDaoSql::PatientDaoSql(Manager *manager) :
    PatientDao(manager)
{

}

PatientList PatientDaoSql::getAllPatients()
{
    PatientList l;

    QSqlQuery q;
    bool success = q.exec("select idPatient, pID, name from patient");

    if (!success)
    {
        QLOG_ERROR() << "PatientDaoSql::getAllPatients - " << util::SqlHelper::formatSqlError(q.lastError());
        return l;
    }

    QLOG_INFO() << "PatientDaoSql::getAllPatients - QuerySize " << util::SqlHelper::getQuerySizeFormatted(q, "Patients");

    while (q.next())
    {
        l.push_back(this->createPatient(q));
    }

    return l;
}

avpp::ent::Patient* PatientDaoSql::getPatientStubByStudy(avpp::ent::Study* study)
{
    QSqlQuery q;
    q.prepare("select idPatient from patient, study where idPatient = Patient_idPatient and idStudy = :idStudy");
    q.bindValue(":idStudy", study->getId());

    bool success = q.exec();

    ent::Patient* patient = nullptr;
    if (success)
    {
        if (q.next())
        {
            QLOG_INFO() << "PatientDaoSql::getPatientStubByStudy - patient stub for study (#" << study->getId() << ") successfully loaded";
            patient = new ent::Patient(q.value(0).toInt(), this->getManager());
        }
        else
        {
            QLOG_WARN() << "PatientDaoSql::getPatientStubByStudy - empty resultset retrieved for patient stub for study (#" << study->getId() << ")";
        }
    }
    else
    {
        QLOG_ERROR() << "PatientDaoSql::getPatientStubByStudy - " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return patient;
}

avpp::ent::Patient* PatientDaoSql::getPatientById(int id)
{
    QSqlQuery q;
    q.prepare("select idPatient, pID, name from patient where idPatient = :idPatient");
    q.bindValue(":idPatient", id);

    bool success = q.exec();

    ent::Patient* patient = nullptr;
    if (success)
    {
        if (q.next())
        {
            QLOG_INFO() << "PatientDaoSql::getPatientById - patient (#" << id << ") successfully loaded";
            patient = this->createPatient(q);
        }
        else
        {
            QLOG_WARN() << "PatientDaoSql::getPatientById - empty resultset retrieved for patient (#" << id << ")";
        }
    }
    else
    {
        QLOG_ERROR() << "PatientDaoSql::getPatientById - " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return patient;
}

avpp::ent::Patient* PatientDaoSql::getPatientByPid(QString pID)
{
	QSqlQuery q;
	q.prepare("select idPatient from patient where pID = :pID");
	q.bindValue(":pID", pID);

	bool success = q.exec();

	int idPatient = -1;
	if (success)
	{
		if (q.next())
		{
			QLOG_INFO() << "PatientDaoSql::getPatientByPid - patient successfully detected by pID:" << pID;
            idPatient = q.value(0).toInt();
        }
        else
        {
            QLOG_WARN() << "PatientDaoSql::getPatientByPid - empty resultset retrieved for pID:" << pID;
        }
    }
    else
    {
        QLOG_ERROR() << "PatientDaoSql::getPatientByPid - " << util::SqlHelper::formatSqlError(q.lastError());
    }

	return this->getManager()->getPatientById(idPatient);
}

avpp::ent::Patient* PatientDaoSql::getPatientByStudy(avpp::ent::Study* study)
{
    QSqlQuery q;
    q.prepare("select idPatient, pID, name from patient, study where idPatient = Patient_idPatient and idStudy = :idStudy");
    q.bindValue(":idStudy", study->getId());

    bool success = q.exec();

    ent::Patient* patient = nullptr;
    if (success)
    {
        if (q.next())
        {
            QLOG_INFO() << "PatientDaoSql::getPatientByStudy - Patient successfully retrieved by Study (#" << study->getId() << ")";
            patient = this->createPatient(q);
        }
        else
        {
            QLOG_WARN() << "PatientDaoSql::getPatientByStudy - empty resultset retrieved for patien by study (#" << study->getId() << ")";
        }
    }
    else
    {
        QLOG_ERROR() << "PatientDaoSql::getPatientByStudy - " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return patient;
}

bool PatientDaoSql::insertPatient(avpp::ent::Patient* patient)
{
    QSqlQuery q;
    q.prepare("insert into patient (pID, name) value(:pID, :name)");
    q.bindValue(":pID", patient->getPID());
    q.bindValue(":name", patient->getName());

    bool success = q.exec();

    if (success)
    {
        q.next();
        patient->setId(util::SqlHelper::getLastInsertedId(q));
        QLOG_INFO() << "PatientDaoSql::insertPatient - Patient " << patient->getId() << " successfully inserted";
    }
    else
    {
        QLOG_ERROR() << "PatientDaoSql::insertPatient - Patient " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return success;
}

bool PatientDaoSql::updatePatient(avpp::ent::Patient* patient)
{
    QSqlQuery q;
    q.prepare("update patient set pID = :pID, name = :name where idPatient = :idPatient");
    q.bindValue(":pID", patient->getPID());
    q.bindValue(":name", patient->getName());
    q.bindValue(":idPatient", patient->getId());

    bool success = q.exec();

    if (success)
    {
        QLOG_INFO() << "PatientDaoSql::updatePatient - Patient " << patient->getId() << " successfully updated";
    }
    else
    {
        QLOG_ERROR() << "PatientDaoSql::updatePatient - Patient " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return success;
}

bool PatientDaoSql::deletePatient(avpp::ent::Patient* patient)
{
    QSqlQuery q;
    q.prepare("delete from patient where idPatient = :idPatient");
    q.bindValue(":idPatient", patient->getId());

    bool success = q.exec();

    if (success)
    {
        QLOG_INFO() << "PatientDaoSql::deletePatient - Patient " << patient->getId() << " successfully deleted";
    }
    else
    {
        QLOG_ERROR() << "PatientDaoSql::deletePatient - Patient " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return success;
}

DAO_TYPE PatientDaoSql::getDaoType()
{
    return SQL;
}

ent::Patient* PatientDaoSql::createPatient(QSqlQuery q)
{
    return new ent::Patient(
                q.value(0).toInt(), // id
                q.value(1).toString(), // pID
                q.value(2).toString(), // name
                this->getManager()
                );
}

#pragma once

#include "PatientDao.h"

class QSqlQuery;

namespace avpp
{
    namespace ent
    {
        class Study;
    }

    class Manager;

    namespace dao
    {
        class PatientDaoSql : public PatientDao
        {
        public:
            PatientDaoSql(Manager* manager);
            virtual ~PatientDaoSql() {}

            PatientList getAllPatients();
            avpp::ent::Patient* getPatientStubByStudy(avpp::ent::Study* study);
            avpp::ent::Patient* getPatientById(int id);
            avpp::ent::Patient* getPatientByPid(QString pID);
            avpp::ent::Patient* getPatientByStudy(avpp::ent::Study* study);
            bool insertPatient(avpp::ent::Patient* patient);
            bool updatePatient(avpp::ent::Patient* patient);
            bool deletePatient(avpp::ent::Patient* patient);
            DAO_TYPE getDaoType();

        private:
            ent::Patient* createPatient(QSqlQuery q);
        };
    }
}

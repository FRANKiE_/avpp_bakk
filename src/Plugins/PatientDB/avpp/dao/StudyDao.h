#pragma once

#include "../Avpp.h"
#include "Dao.h"

namespace avpp
{
    class Manager;

    namespace dao
    {
        class StudyDao : public Dao
        {
        public:
            StudyDao(Manager* manager);
            virtual ~StudyDao() {}

            virtual StudyList getAllStudiesByPatient(avpp::ent::Patient* patient) = 0;
            virtual StudyList getAllStudyStubsByPatient(avpp::ent::Patient* patient) = 0;
            virtual avpp::ent::Study* getStudyStubBySeries(avpp::ent::Series* series) = 0;
            virtual avpp::ent::Study* getStudyById(int id) = 0;
            virtual avpp::ent::Study* getStudyByUid(QString uID) = 0;
            virtual bool insertStudy(avpp::ent::Study* study) = 0;
            virtual bool updateStudy(avpp::ent::Study* study) = 0;
            virtual bool deleteStudy(avpp::ent::Study* study) = 0;
        };
    }
}

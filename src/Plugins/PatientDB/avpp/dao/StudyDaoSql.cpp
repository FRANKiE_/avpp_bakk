#include "StudyDaoSql.h"
#include "../ent/Study.h"
#include "../ent/Series.h"
#include "../ent/Patient.h"
#include "../util/sqlhelper.h"

#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlError>

using namespace avpp;
using namespace avpp::dao;

StudyDaoSql::StudyDaoSql(Manager *manager) :
    StudyDao(manager)
{

}

StudyList StudyDaoSql::getAllStudiesByPatient(avpp::ent::Patient* patient)
{
    StudyList l;

    QSqlQuery q;
    q.prepare("select idStudy, uid, description, date, time from study where Patient_idPatient = :Patient_idPatient");
    q.bindValue(":Patient_idPatient", patient->getId());
    bool success = q.exec();

    if (!success)
    {
        QLOG_ERROR() << "StudyDaoSql::getAllStudiesByPatient - " << util::SqlHelper::formatSqlError(q.lastError());
        return l;
    }

    QLOG_INFO() << "StudyDaoSql::getAllStudiesByPatient - " << util::SqlHelper::getQuerySizeFormatted(q, "Studies");

    while (q.next())
    {
        l.push_back(this->createStudy(q));
    }

    return l;
}

StudyList StudyDaoSql::getAllStudyStubsByPatient(avpp::ent::Patient* patient)
{
    StudyList l;

    QSqlQuery q;
    q.prepare("select idStudy from study where patient_idpatient = :patient_idpatient");
    q.bindValue(":patient_idpatient", patient->getId());
    bool success = q.exec();

    if (!success)
    {
        QLOG_ERROR() << "StudyDaoSql::getAllStudyStubsByPatient - " << util::SqlHelper::formatSqlError(q.lastError());
        return l;
    }

    QLOG_INFO() << "StudyDaoSql::getAllStudyStubsByPatient - QuerySize " << util::SqlHelper::getQuerySizeFormatted(q, "Studies");

    while (q.next())
    {
        ent::Study* study = new ent::Study(
                q.value(0).toInt(), // id
                this->getManager()
                );
        study->setPatient(patient);
        l.push_back(study);
    }

    return l;
}

ent::Study* StudyDaoSql::getStudyStubBySeries(ent::Series *series)
{
    QSqlQuery q;
    q.prepare("select idStudy from study, series where idStudy = Study_idStudy and idSeries = :idSeries");
    q.bindValue(":idSeries", series->getId());

    bool success = q.exec();

    ent::Study* study = nullptr;
    if (success)
    {
        if (q.next())
        {
            QLOG_INFO() << "StudyDaoSql::getStudyStubBySeries - study stub for series (#" << series->getId() << ") successfully loaded";
            study = new ent::Study(q.value(0).toInt(), this->getManager());
        }
        else
        {
            QLOG_WARN() << "StudyDaoSql::getStudyStubBySeries - empty resultset retrieved for study stub for series (#" << series->getId() << ")";
        }
    }
    else
    {
        QLOG_ERROR() << "StudyDaoSql::getStudyStubBySeries - " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return study;
}

ent::Study* StudyDaoSql::getStudyById(int id)
{
    QSqlQuery q;
    q.prepare("select idStudy, uid, description, date, time from study where idStudy = :idStudy");
    q.bindValue(":idStudy", id);

    bool success = q.exec();

    ent::Study* study = nullptr;
    if (success)
    {
        if (q.next())
        {
            QLOG_INFO() << "StudyDaoSql::getStudyById - study (#" << id << ") successfully loaded";
            study = this->createStudy(q);
        }
        else
        {
             QLOG_WARN() << "FileDaoSql::getStudyById - empty resultset retrieved for study (#" << id << ")";
        }
    }
    else
    {
        QLOG_ERROR() << "StudySqoSql::getStudyById - " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return study;
}

ent::Study* StudyDaoSql::getStudyByUid(QString uID)
{
    QSqlQuery q;
    q.prepare("select idStudy from study where uID = :uID");
    q.bindValue(":uID", uID);

    bool success = q.exec();

	int idStudy = -1;
    if (success)
    {
        if (q.next())
        {
            QLOG_INFO() << "StudyDaoSql::getStudyByUid - study successfully detected by uID:" << uID;
			idStudy = q.value(0).toInt();
        }
        else
        {
             QLOG_WARN() << "FileDaoSql::getStudyByUid - empty resultset retrieved for uID:" << uID;
        }
    }
    else
    {
        QLOG_ERROR() << "StudySqoSql::getStudyByUid - " << util::SqlHelper::formatSqlError(q.lastError());
    }

	return this->getManager()->getStudyById(idStudy);
}

bool StudyDaoSql::insertStudy(avpp::ent::Study* study)
{
    if (study->getPatient() == nullptr)
    {
        QLOG_ERROR() << "StudyDaoSql::insertStudy - Study getPatient is null";
        return false;
    }

    QSqlQuery q;
    q.prepare("insert into study (uid, description, date, time, Patient_idPatient) value(:uid, :description, :date, :time, :Patient_idPatient)");
    q.bindValue(":uid", study->getUID());
    q.bindValue(":description", study->getDescription());
    q.bindValue(":date", study->getDate());
    q.bindValue(":time", study->getTime());
    QVariant idPatient = study->getPatient() == nullptr ? QVariant(QVariant::Int) : study->getPatient()->getId();
    q.bindValue(":Patient_idPatient", idPatient);

    bool success = q.exec();

    if (success)
    {
        study->setId(util::SqlHelper::getLastInsertedId(q));
        QLOG_INFO() << "StudyDaoSql::insertStudy - successfully inserted study (#" << study->getId() << ")";
    }
    else
    {
        QLOG_ERROR() << "StudyDaoSql::insertStudy - " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return success;
}

bool StudyDaoSql::updateStudy(avpp::ent::Study* study)
{
    if (study->getPatient() == nullptr)
    {
        QLOG_WARN() << "StudyDaoSql::updateStudy - could not update study (#" << study->getId() << ") no patient referenced";
        return false;
    }

    QSqlQuery q;
    q.prepare("update study set uid = :uid, description = :description, date = :date, time = :time, Patient_idPatient = :Patient_idPatient where idStudy = :idStudy");
    q.bindValue(":uid", study->getUID());
    q.bindValue(":description", study->getDescription());
    q.bindValue(":date", study->getDate());
    q.bindValue(":time", study->getTime());
    q.bindValue(":idStudy", study->getId());
    QVariant idPatient = study->getPatient() == nullptr ? QVariant(QVariant::Int) : study->getPatient()->getId();
    q.bindValue(":Patient_idPatient", idPatient);

    bool success = q.exec();

    if (success)
    {
        QLOG_INFO() << "StudyDaoSql::updateStudy - Study " << study->getId() << " successfully updated";
    }
    else
    {
        QLOG_ERROR() << "StudyDaoSql::updateStudy - Study " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return success;
}

bool StudyDaoSql::deleteStudy(avpp::ent::Study* study)
{
    QSqlQuery q;
    q.prepare("delete from study where idStudy = :idStudy");
    q.bindValue(":idStudy", study->getId());

    bool success = q.exec();

    if (success)
    {
        QLOG_INFO() << "StudyDaoSql::deleteStudy - Study " << study->getId() << " successfully deleted";
    }
    else
    {
        QLOG_ERROR() << "StudyDaoSql::deleteStudy - Study " << util::SqlHelper::formatSqlError(q.lastError());
    }

    return success;
}

DAO_TYPE StudyDaoSql::getDaoType()
{
    return SQL;
}

ent::Study* StudyDaoSql::createStudy(QSqlQuery q)
{
    ent::Study* study = new ent::Study(
                q.value(0).toInt(), // idStudy
                q.value(1).toString(), // uid
                q.value(2).toString(), // description
                q.value(3).toDate(), // date
                q.value(4).toTime(), // time
                this->getManager()
                );

    study->setPatient(this->getManager()->getPatientByStudy(study, ent::LAZY));
    return study;
}

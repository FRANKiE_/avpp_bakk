#pragma once

#include "StudyDao.h"

class QSqlQuery;

namespace avpp
{
    class Manager;

    namespace dao
    {
        class StudyDaoSql : public StudyDao
        {
        public:
            StudyDaoSql(Manager* manager);
            virtual ~StudyDaoSql() {}

            StudyList getAllStudiesByPatient(avpp::ent::Patient* patient);
            StudyList getAllStudyStubsByPatient(avpp::ent::Patient* patient);
            avpp::ent::Study* getStudyStubBySeries(avpp::ent::Series* series);
            avpp::ent::Study* getStudyById(int id);
            avpp::ent::Study* getStudyByUid(QString uID);
            bool insertStudy(avpp::ent::Study* study);
            bool updateStudy(avpp::ent::Study* study);
            bool deleteStudy(avpp::ent::Study* study);
            DAO_TYPE getDaoType();

        private:
            ent::Study* createStudy(QSqlQuery q);
        };
    }
}

#include "Entity.h"

using namespace avpp;
using namespace avpp::ent;


Entity::Entity(Manager* _manager) :
    manager(_manager),
    status(STUB),
    id(0)
{

}

Entity::~Entity()
{
}

int Entity::getId()
{
    return this->id;
}

void Entity::setId(int id)
{
    this->id = id;
}

EntityStatus Entity::getStatus()
{
    return this->status;
}

void Entity::setLoaded()
{
    this->status = LOADED;
}

void Entity::setDirty()
{
    this->status = DIRTY;
}

bool Entity::comparePredicate(Entity* entity)
{
    return this->getId() == entity->getId();
}

Manager* Entity::getManager()
{
    return this->manager;
}

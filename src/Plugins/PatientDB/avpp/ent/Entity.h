#pragma once

#include "Avpp.h"
#include "Manager.h"

namespace avpp
{
    namespace ent
    {
        class Entity
        {
        public:
            Entity(Manager* manager);
            virtual ~Entity();

            int getId();
            void setId(int id);

            EntityStatus getStatus();
            void setLoaded();
            void setDirty();

            bool comparePredicate(Entity* entity);

        private:
            Manager* manager;
            EntityStatus status;
            int id;

        protected:
             Manager* getManager();

        };
    }
}

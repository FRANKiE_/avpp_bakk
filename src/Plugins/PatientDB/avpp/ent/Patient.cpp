#include "Patient.h"

using namespace avpp;
using namespace avpp::ent;

Patient::Patient(int id, Manager* manager) :
    Entity(manager)
{
    Entity::setId(id);
}

Patient::Patient(QString pID, QString name, Manager* manager) :
    Entity(manager)
{
    this->pID = pID;
    this->name = name;
    this->setDirty();
}

Patient::Patient(int id, QString pID, QString name, Manager* manager) :
    Entity(manager)
{
    Entity::setId(id);
    this->pID = pID;
    this->name = name;
    this->setDirty();
}

Patient::~Patient()
{
}

bool Patient::update(Patient* patient)
{
    if (this->getId() != patient->getId())
    {
        QLOG_WARN() << "Patient::update - could not update patient (#" << patient->getId() << ") ids did not match";
        return false;
    }

    this->setName(patient->getName());

    return true;
}

QString Patient::getPID()
{
    return this->pID;
}

void Patient::setPID(QString pID)
{
	this->setDirty();
	this->pID = pID;
}

QString Patient::getName()
{
    return this->name;
}

void Patient::setName(QString name)
{
    this->setDirty();
    this->name = name;
}

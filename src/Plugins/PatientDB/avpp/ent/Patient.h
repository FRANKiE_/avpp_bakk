#pragma once

#include "Entity.h"
#include <QString>

namespace avpp
{
    class Manager;

    namespace ent
    {
        class Patient : public Entity
        {
        public:
            /**
             * @brief Patient
             * @param id
             * @param manager
             */
            Patient(int id, Manager* manager);

            /**
             * @brief Patient Constructor to create a new patient, not known to the system
             * @param pID
             * @param name
             * @param manager
             */
            Patient(QString pID, QString name, Manager* manager);

            /**
             * @brief Patient Constructor to create a patient, loaded from the database
             * @param id
             * @param pID
             * @param name
             * @param manager
             */
            Patient(int id, QString pID, QString name, Manager* manager);

            virtual ~Patient();

            bool update(Patient* patient);

            QString getPID();
			void setPID(QString pID);
            QString getName();
            void setName(QString name);

        private:
            QString pID;
            QString name;
        };
    }
}

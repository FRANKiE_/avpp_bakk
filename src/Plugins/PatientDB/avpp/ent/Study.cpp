#include "Study.h"
#include "Patient.h"
#include "Manager.h"

#include <algorithm>

using namespace avpp;
using namespace avpp::ent;

Study::Study(int id, Manager* manager) :
    Entity(manager),
    patient(nullptr)
{
    Entity::setId(id);
}

Study::Study(QString uid, QString description, QDate date, QTime time, Manager* manager) :
    Entity(manager),
    patient(nullptr)
{
    this->uid = uid;
    this->description = description;
    this->date = date;
    this->time = time;
    this->setDirty();
}

Study::Study(int id, QString uid, QString description, QDate date, QTime time, Manager* manager) :
    Entity(manager),
    patient(nullptr)
{
    Entity::setId(id);
    this->uid = uid;
    this->description = description;
    this->date = date;
    this->time = time;
    this->setDirty();
}

Study::~Study()
{
}

bool Study::update(Study* study)
{
    if (this->getId() != study->getId())
    {
        QLOG_WARN() << "Study::update - could not update study (#" << study->getId() << ") ids did not match";
        return false;
    }

    this->setUID(study->getUID());
    this->setDescription(study->getDescription());
    this->setDate(study->getDate());
    this->setTime(study->getTime());
    this->setPatient(study->getPatient());

    return true;
}

QString Study::getUID()
{
    return this->uid;
}

void Study::setUID(QString uid)
{
    this->uid = uid;
}

QString Study::getDescription()
{
    return this->description;
}

void Study::setDescription(QString description)
{
    this->description = description;
}

QDate Study::getDate()
{
    return this->date;
}

void Study::setDate(QDate date)
{
    this->date = date;
}

QTime Study::getTime()
{
    return this->time;
}

void Study::setTime(QTime time)
{
    this->time = time;
}

Patient* Study::getPatient()
{
    return this->patient;
}

void Study::setPatient(Patient* patient)
{
    // new patient is different from currently referenced patient
    if (this->patient != nullptr && this->patient->getId() != patient->getId())
    {
        // search for this study in previous patient and remove it from matches
        StudyList sl = this->getManager()->patientStudyMatches[this->patient->getId()];
        StudyList::iterator it = std::find_if(sl.begin(),
                                            sl.end(),
                                            std::bind1st(std::mem_fun(&ent::Entity::comparePredicate), this)
                                            );
        sl.erase(it);

        // add this study to the new patient's matches
        sl = this->getManager()->patientStudyMatches[patient->getId()];
        sl.push_back(this);
    }

    this->patient = patient;
}

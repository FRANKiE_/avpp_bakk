#pragma once

#include "Entity.h"
#include <QString>
#include <QDate>
#include <QTime>

namespace avpp
{
    namespace ent
    {
        class Study : public Entity
        {
        public:
            /**
             * @brief Study
             * @param id
             * @param manager
             */
            Study(int id, Manager* manager);

            /**
             * @brief Study Constructor to create a new study, not known to the system
             * @param uid
             * @param description
             * @param date
             * @param time
             * @param manager
             */
            Study(QString uid, QString description, QDate date, QTime time, Manager* manager);

            /**
             * @brief Study Constructor to create a study, loaded from the database
             * @param id
             * @param uid
             * @param description
             * @param date
             * @param time
             * @param manager
             */
            Study(int id, QString uid, QString description, QDate date, QTime time, Manager* manager);

            virtual ~Study();

            bool update(Study* study);

            QString getUID();
            void setUID(QString uid);
            QString getDescription();
            void setDescription(QString description);
            QDate getDate();
            void setDate(QDate date);
            QTime getTime();
            void setTime(QTime time);
            Patient* getPatient();
            void setPatient(Patient* patient);

        private:
            QString uid;
            QString description;
            QDate date;
            QTime time;
            Patient* patient;

        };
    }
}

#include <cstdlib>
#include <ctime>

#include "Manager.h"
#include "bin/FileManager.h"
#include "dao/FileDao.h"
#include "dao/FileTypeDao.h"
#include "dao/PatientDao.h"
#include "dao/SeriesDao.h"
#include "dao/StudyDao.h"
#include "ent/Entity.h"
#include "ent/File.h"
#include "ent/FileType.h"
#include "ent/Patient.h"
#include "ent/Series.h"
#include "ent/Study.h"
#include "util/Settings.h"
#include "util/qslog/QsLog.h"

#include "osconfig.h"
#include "dcdeftag.h"

#include "dctk.h"          /* for various dcmdata headers */
//#include "dcutils.h"       /* for getSingleValue */
#include "dcdebug.h"       /* for SetDebugLevel */
#include "cmdlnarg.h"      /* for prepareCmdLineArgs */
#include "dcuid.h"         /* for dcmtk version name */

#include "dcmimage.h"      /* for DicomImage */
#include "digsdfn.h"       /* for DiGSDFunction */
#include "diciefn.h"       /* for DiCIELABFunction */

#include "ofconapp.h"      /* for OFConsoleApplication */
#include "ofcmdln.h"       /* for OFCommandLine */
#include "didocu.h"
#include "dcistrmf.h"

using namespace avpp;
using namespace avpp::ent;

void loadBinaryDataAndMetaInfoFromDICOMFile(const QString& fileName, avpp::Manager* m)
{
	E_TransferSyntax oxfer = EXS_LittleEndianExplicit;
	E_EncodingType oenctype = EET_ExplicitLength;
	E_GrpLenEncoding oglenc = EGL_recalcGL;
	E_PaddingEncoding opadenc = EPD_withoutPadding;
	OFCmdUnsignedInt opt_filepad = 0;
	OFCmdUnsignedInt opt_itempad = 0;

	DcmFileFormat * fileformat = NULL;
	DcmMetaInfo * metaheader = NULL;
	DcmDataset * dataset = NULL;

	if (fileName.isEmpty() || fileName.isNull()) {
		return;
	}

	DcmInputFileStream iStream(fileName.toLatin1().constData());
	if (!iStream.good()) {
		return;
	}

	// Create dicom metaheader and dataset
	OFBool memoryError = OFFalse;

	fileformat = new DcmFileFormat();
	if (!fileformat){   
		return;
	}

	OFCondition l_error = EC_Normal;

	fileformat->transferInit();
	l_error = fileformat->read(iStream);
	fileformat->transferEnd();


	metaheader = fileformat->getMetaInfo();
	dataset = fileformat->getDataset();

	DcmElement* el;

	int idx = 0;   
	char* val;

	bool insertPatient = true;
	avpp::ent::Patient* p = nullptr;
	bool insertStudy = true;
	avpp::ent::Study* st = nullptr;
	bool insertSeries = true;
	avpp::ent::Series* se = nullptr;

	while ((el = dataset->getElement(idx++)) != 0)
	{
		el->getString(val);

		if (el->getTag() == DCM_PatientsName)
		{
			if (p == nullptr)
			{
				p = new avpp::ent::Patient("", QString(val), m);
			}
			else
			{
				p->setName(QString(val));
			}
		}

		if (el->getTag() == DCM_PatientID)
		{
			QString pID = QString(val);
			if (p == nullptr)
			{
				p = new avpp::ent::Patient(pID, "", m);
			}
			else
			{
				p->setPID(pID);
			}

			avpp::ent::Patient* p2 = m->getPatientByPid(pID);
			if (p2 != nullptr)
			{
				insertPatient = false;
				p2->setName(p->getName());
				m->updatePatient(p2);
				p = p2;
			}
		}

		if (el->getTag() == DCM_StudyDate)
		{
			QString dateStr = QString(val);
			int year = dateStr.left(4).toInt();
			int month = dateStr.mid(4, 2).toInt();
			int day = dateStr.right(2).toInt();
			QDate date = QDate(year, month, day);

			if (st == nullptr)
			{
				st = new avpp::ent::Study("", "", date, QTime(), m);
			}
			else
			{
				st->setDate(date);
			}
		}

		if (el->getTag() == DCM_StudyTime)
		{
			QString timeStr = QString(val);
			int hour = timeStr.section(".", 0, 0).left(2).toInt();
			int minute = timeStr.section(".", 0, 0).mid(2, 2).toInt();
			int second = timeStr.section(".", 0, 0).right(2).toInt();
			int millisecond = timeStr.section(".", 1, 1).toInt();
			QTime time = QTime(hour, minute, second, millisecond);

			if (st == nullptr)
			{
				st = new avpp::ent::Study("", "", QDate(), time, m);
			}
			else
			{
				st->setTime(time);
			}
		}

		if (el->getTag() == DCM_StudyDescription)
		{
			if (st == nullptr)
			{
				st = new avpp::ent::Study("", QString(val), QDate(), QTime(), m);
			}
			else
			{
				st->setDescription(QString(val));
			}
		}

		if (el->getTag() == DCM_StudyInstanceUID)
		{
			QString uID = QString(val);
			if (st == nullptr)
			{
				st = new avpp::ent::Study(uID, "", QDate(), QTime(), m);
			}
			else
			{
				st->setUID(uID);
			}

			avpp::ent::Study* st2 = m->getStudyByUid(uID);
			if (st2 != nullptr)
			{
				insertStudy = false;
				st2->setDate(st->getDate());
				st2->setDescription(st->getDescription());
				st2->setTime(st->getTime());
				m->updateStudy(st2);
				st = st2;
			}
		}

		if (el->getTag() == DCM_SeriesDescription)
		{
			if (se == nullptr)
			{
				se = new avpp::ent::Series("", QString(val), QDate(), QTime(), m);
			}
			else
			{
				se->setDescription(QString(val));
			}
		}

		if (el->getTag() == DCM_SeriesInstanceUID)
		{
			QString uID = QString(val);
			if (se == nullptr)
			{
				se = new avpp::ent::Series(uID, "", QDate(), QTime(), m);
			}
			else
			{
				se->setUID(uID);
			}

			avpp::ent::Series* se2 = m->getSeriesByUid(uID);
			if (se2 != nullptr)
			{
				insertSeries = false;
				se2->setDate(se->getDate());
				se2->setDescription(se->getDescription());
				se2->setTime(se->getTime());
				m->updateSeries(se2);
				se = se2;
			}
		}
	} // while(...

	if (insertPatient)
	{
		m->insertPatient(p);
	}

	if (st != nullptr)
	{
		st->setPatient(p);
		
		if (insertStudy)
		{
			m->insertStudy(st);
		}
		else
		{
			m->updateStudy(st);
		}
	}

	if (se != nullptr)
	{
		se->setStudy(st);

		if (insertSeries)
		{
			m->insertSeries(se);
		}
		else
		{
			m->updateSeries(se);
		}
	}

	avpp::ent::File* f = new avpp::ent::File(fileName, m->getFileTypeByName("DICOM"), m);
	if (f != nullptr)
	{
		f->setSeries(se);
		m->insertFile(f);
	}
}



int main(void)
{
	avpp::Manager* m = avpp::Manager::getInstance();
	QLOG_INFO() << "starttime: " << time(0);
	QString path = "C:/Users/manu/Documents/projects/avp2qt434/";
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0001.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0002.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0003.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0004.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0005.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0006.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0007.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0008.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0009.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0010.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0011.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0012.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0013.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0014.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0015.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0016.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0017.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0018.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0019.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0020.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0021.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0022.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0023.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0024.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0025.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0026.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0027.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0028.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0029.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0030.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0031.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0032.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0033.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0034.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0035.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0036.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0037.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0038.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0039.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0040.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0041.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0042.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0043.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0044.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0045.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0046.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0047.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0048.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0049.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0050.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0051.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0052.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0053.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0054.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0055.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0056.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0057.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0058.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0059.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0060.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0061.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0062.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0063.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0064.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0065.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0066.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0067.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0068.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0069.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0070.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0071.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0072.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0073.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0074.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0075.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0076.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0077.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0078.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0079.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0080.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0081.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0082.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0083.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0084.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0085.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0086.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0087.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0088.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0089.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0090.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0091.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0092.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0093.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0094.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0095.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0096.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0097.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0098.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0099.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0100.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0101.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0102.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0103.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0104.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0105.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0106.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0107.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0108.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0109.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0110.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0111.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0112.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0113.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0114.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0115.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0116.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0117.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0118.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0119.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0120.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0121.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0122.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0123.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0124.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0125.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0126.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0127.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0128.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0129.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0130.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0131.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0132.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0133.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0134.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0135.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0136.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0137.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0138.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0139.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0140.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0141.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0142.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0143.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0144.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0145.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0146.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0147.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0148.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0149.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0150.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0151.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0152.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0153.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0154.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0155.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0156.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0157.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0158.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0159.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0160.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0161.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0162.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0163.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0164.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0165.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0166.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0167.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0168.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0169.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0170.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0171.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0172.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0173.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0174.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0175.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0176.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0177.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0178.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0179.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0180.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0181.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0182.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0183.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0184.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0185.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0186.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0187.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0188.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0189.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0190.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0191.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0192.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0193.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0194.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0195.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0196.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0197.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0198.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0199.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0200.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0201.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0202.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0203.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0204.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0205.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0206.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0207.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0208.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0209.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0210.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0211.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0212.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0213.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0214.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0215.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0216.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0217.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0218.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0219.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0220.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0221.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0222.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0223.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0224.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0225.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0226.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0227.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0228.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0229.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0230.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0231.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0232.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0233.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0234.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0235.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0236.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0237.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0238.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0239.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0240.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0241.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0242.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0243.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0244.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0245.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0246.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0247.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0248.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0249.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0250.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0251.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0252.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0253.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0254.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0255.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0256.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0257.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0258.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0259.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0260.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0261.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0262.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0263.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0264.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0265.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0266.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0267.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0268.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0269.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0270.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0271.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0272.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0273.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0274.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0275.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0276.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0277.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0278.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0279.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0280.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0281.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0282.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0283.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0284.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0285.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0286.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0287.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0288.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0289.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0290.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0291.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0292.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0293.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0294.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0295.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0296.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0297.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0298.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0299.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0300.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0301.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0302.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0303.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0304.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0305.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0306.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0307.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0308.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0309.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0310.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0311.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0312.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0313.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0314.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0315.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0316.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0317.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0318.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0319.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0320.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0321.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0322.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0323.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0324.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0325.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0326.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0327.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0328.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0329.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0330.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0331.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0332.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0333.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0334.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0335.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0336.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0337.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0338.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0339.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0340.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0341.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0342.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0343.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0344.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0345.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0346.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0347.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0348.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0349.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0350.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0351.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0352.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0353.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0354.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0355.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0356.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0357.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0358.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0359.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0360.dcm"), m);
	loadBinaryDataAndMetaInfoFromDICOMFile(QString(path + "Testing/Data/PHENIX/CT2/OS/IM-0001-0361.dcm"), m);
	QLOG_INFO() << "endtime: " << time(0);
	m->cleanInstance();
	/*
	 * testcase 1
	Series* s3 = m->getSeriesById(3);
	*/
	/*
	 * testcase 2
	Patient* p1 = m->getPatientById(1);
	FileList fl = m->getAllFilesByPatient(p1);
	 */
	/*
	 * testcase 3
	FileList fl = m->getAllFiles();
	 */
	/*
	 * file import
	QString path = "C:/Users/manu/Documents/projects/avf-bakk/src/Testing/Data/";
	avpp::ent::File* f1 = new avpp::ent::File(path + "GD/GD.avw", m->getFileTypeById(3), m);
	m->insertFile(f1);
	avpp::ent::File* f2 = new avpp::ent::File(path + "GD/GD.densityGrid0.f3d", m->getFileTypeById(2), m);
	m->insertFile(f2);
	avpp::ent::File* f3 = new avpp::ent::File(path + "GD/GD.vesselTree.xml", m->getFileTypeById(5), m);
	m->insertFile(f3);
	avpp::ent::File* f4 = new avpp::ent::File(path + "Phantom1/Phantom1.avw", m->getFileTypeById(3), m);
	m->insertFile(f4);
	avpp::ent::File* f5 = new avpp::ent::File(path + "Phantom1/Phantom1.densityGrid0.f3d", m->getFileTypeById(2), m);
	m->insertFile(f5);
	avpp::ent::File* f9 = new avpp::ent::File(path + "Phantom1/Phantom1.vesselTree.xml", m->getFileTypeById(5), m);
	m->insertFile(f9);
	avpp::ent::File* f6 = new avpp::ent::File(path + "Phantom2/Phantom2.avw", m->getFileTypeById(3), m);
	m->insertFile(f6);
	avpp::ent::File* f7 = new avpp::ent::File(path + "Phantom2/Phantom2.densityGrid0.f3d", m->getFileTypeById(2), m);
	m->insertFile(f7);
	avpp::ent::File* f8 = new avpp::ent::File(path + "Phantom2/Phantom2.vesselTree.xml", m->getFileTypeById(5), m);
	m->insertFile(f8);
	 */

	return EXIT_SUCCESS;
}

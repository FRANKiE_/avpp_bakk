#include "Settings.h"
#include "Externals\qjson\include\parser.h"
#include <QFile>
#include "qslog/QsLog.h"

using namespace avpp;
using namespace avpp::util;

Settings::Settings(QString filePath)
{
	this->filePath = filePath;
}

Settings::~Settings()
{
}

bool Settings::load()
{
	QFile file(this->filePath);

	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return false;

	while (!file.atEnd())
	{
		this->jsonData.append(file.readLine());
	}

	file.close();

	QJson::Parser parser;
	bool ok;
	this->result = parser.parse(this->jsonData, &ok);

	if (!ok)
	{
		QLOG_FATAL() << "Settings::load - could not parse config file: " << parser.errorString();
	}

	return ok;
}

QVariant Settings::getResult()
{
	return this->result;
}

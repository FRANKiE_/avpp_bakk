#pragma once

#include <QString>
#include <QVariant>

namespace avpp
{
	namespace util
	{
		class Settings
		{
		public:
			Settings(QString filePath);
			virtual ~Settings();
			bool load();
			QVariant getResult();

		private:
			QString filePath;
			QByteArray jsonData;
			QVariant result;
		};
	}
}

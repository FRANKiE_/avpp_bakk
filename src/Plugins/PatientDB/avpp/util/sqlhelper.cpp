#include "sqlhelper.h"

#include <QSqlError>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QVariant>

using namespace avpp;
using namespace avpp::util;

QString SqlHelper::formatSqlError(QSqlError err)
{
    return QString("QSqlError, #%1, type: %2, text: %3").arg(err.number()).arg(err.type() ).arg(err.text());
}

int SqlHelper::getQuerySize(QSqlQuery query)
{
    QSqlDatabase defDB = QSqlDatabase::database();
    if (defDB.driver()->hasFeature(QSqlDriver::QuerySize))
    {
        return query.size();
    }
    else
    {
        return -1;
    }
}

QString SqlHelper::getQuerySizeFormatted(QSqlQuery query, QString entName)
{
    QSqlDatabase defDB = QSqlDatabase::database();
    int size = SqlHelper::getQuerySize(query);
    if (size != -1)
    {
        return QString("received %1 %2").arg(size).arg(entName);
    }
    else
    {
        return QString("QuerySize feature is not available for database %1").arg(defDB.driverName());
    }
}

int SqlHelper::getLastInsertedId(QSqlQuery query)
{
    QSqlDatabase defDB = QSqlDatabase::database();
    if (defDB.driver()->hasFeature(QSqlDriver::LastInsertId))
    {
        return query.lastInsertId().toInt();
    }
    else
    {
        QLOG_FATAL() << "SqlHelper::getLastInsertedId - could no retrieve last inserted id";
        return -1;
    }
}

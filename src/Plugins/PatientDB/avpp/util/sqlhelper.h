#pragma once

#include "qslog/QsLog.h"

class QSqlQuery;
class QSqlError;
class QString;

namespace avpp
{
    namespace util
    {
        class SqlHelper
        {
        public:
            static QString formatSqlError(QSqlError err);
            static int getQuerySize(QSqlQuery query);
            static QString getQuerySizeFormatted(QSqlQuery query, QString entName);
            static int getLastInsertedId(QSqlQuery query);
        };
    }
}
